package Controler;

import Model.Automaton;
import Model.CellStateFactory;
import Model.GameOfLife;
import Model.LangtonAnt;
import Model.MoorNeighborhood;
import Model.NeighborhoodFactory;
import Model.Rule;
import Model.VonNeumanNeighborhood;
import Model.WireWorld;
import View.StartWindow;
/**
 * Fabryka gier
 * @author Dominik Katszer
 *
 */
public class StartDataGamesFactory {
	/**
	 * getter gry
	 * @param cellStateFactory obiekt inicjalizujacy
	 * @return zerowy stan gry
	 */
	public static Automaton getGame( CellStateFactory cellStateFactory){
		StartDataBase data = StartWindow.startData;
		String[] variant = data.getVariant().split("/");
		String game = data.getGame();
		
		if(game=="Game of Live"){	
			if(data.getVariant()=="quad life"){
				int[] stayAlive = {2,3};
				int[] becomeLive = {3};
				return new GameOfLife(data.getWidth(), data.getHeight(),   cellStateFactory ,NeighborhoodFactory.getNeighborhood(StartWindow.startData.isMoore()),stayAlive ,becomeLive);
			}
			else{
				int[] stayAlive = new int[variant[0].length()];
				int[] becomeLive = new int[variant[1].length()];

				for(int i = 0 ; i <variant[0].length() ; i++){
					stayAlive[i]=Integer.parseInt(Character.toString(variant[0].charAt(i)));
				}
				for(int i = 0 ; i <variant[1].length() ; i++){
					becomeLive[i]=Integer.parseInt(Character.toString(variant[1].charAt(i)));
				}
			
			return new GameOfLife(data.getWidth(), data.getHeight(),   cellStateFactory ,NeighborhoodFactory.getNeighborhood(StartWindow.startData.isMoore()),stayAlive ,becomeLive);
			}
		}
		else if(game=="Langton Ant"){
			return new LangtonAnt(data.getWidth(), data.getHeight(),   cellStateFactory ,new VonNeumanNeighborhood(1,StartWindow.startData.isWrapping(), StartWindow.startData.getWidth(), StartWindow.startData.getHeight()));
		}
		else if(game=="Wireworld"){
			//JOptionPane.showMessageDialog(null,StartWindow.startData.isWrapping());
			return new WireWorld(data.getWidth(), data.getHeight(),   cellStateFactory ,new MoorNeighborhood(1,StartWindow.startData.isWrapping(), StartWindow.startData.getWidth(), StartWindow.startData.getHeight()));
		}
		else if(game=="Rule"){		
			return new Rule(data.getWidth(), cellStateFactory, new MoorNeighborhood(1, StartWindow.startData.isWrapping(),StartWindow.startData.getWidth(), StartWindow.startData.getHeight()), Integer.parseInt(StartWindow.startData.getVariant()));
		}
		return null;
	}
}
