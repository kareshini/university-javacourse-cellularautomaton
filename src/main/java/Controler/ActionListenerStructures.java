package Controler;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import Model.BinaryState;
import Model.Coords2D;
import Model.QuadState;
import View.GameStatesFactory;
import View.StartWindow;
/**
 * Fabryka akcji przy kliknieciu wyboru struktur w mainWindow 
 * @author Dominik Katszer
 *
 */
public class ActionListenerStructures {
	private static int w,h;
	private static JButton[] tab;
	private static String game;
	private static JComboBox<String> structureBox;
	/**
	 * getter
	 * @param comboBox ComboBox struktur
	 * @param buttonsTab plansza gry
	 * @return akcje przy wyborze
	 */
	public static ActionListener getActionListener(JComboBox<String> comboBox, JButton[] buttonsTab){
		w=StartWindow.startData.getWidth();
		h=StartWindow.startData.getHeight();
		tab=buttonsTab;
		game=StartWindow.startData.getGame();
		structureBox=comboBox;
		
		if(game=="Game of Live"){	
			return getGolListener();
		}
		else if(game=="Langton Ant"){

		}
		else if(game=="Wireworld"){
		}
		else if(game=="Rule"){	
		}
	
		return new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		};
	}
	private static void clear(){
		for(JButton el : tab){
			el.setBackground(GameStatesFactory.getStates(game).get(GameStatesFactory.getStartedState(game)));
		}
	}
	private static ActionListener getGolListener(){
		return new ActionListener(){
			int center_x=w/2;
			int center_y=h/2;//dolny prawy rog
			@Override
			public void actionPerformed(ActionEvent e) {
				clear();
				Set<Coords2D> set = new HashSet<Coords2D>();
			//	JOptionPane.showMessageDialog(null,(String)structureBox.getSelectedItem());
				switch((String)structureBox.getSelectedItem()){
				case "Block":
					if(w<2 || h<2){
						JOptionPane.showMessageDialog(null,"Board is too small");
					}
					else{
					//	JOptionPane.showMessageDialog(null,"IN BLOCK");
						set.add(new Coords2D(center_x,center_y));
						set.add(new Coords2D(center_x - 1,center_y - 1));
						set.add(new Coords2D(center_x - 1,center_y));
						set.add(new Coords2D(center_x,center_y - 1));
					}
					break;
				case "Blinker":
					if(w<3 || h<3){
						JOptionPane.showMessageDialog(null,"Board is too small");
					}
					else{
					//	JOptionPane.showMessageDialog(null,"IN BLINKER");
						set.add(new Coords2D(center_x,center_y));
						set.add(new Coords2D(center_x,center_y - 1));
						set.add(new Coords2D(center_x,center_y+1));
					}
					break;
				case "Glider":
					if(w<3 || h<3){
						JOptionPane.showMessageDialog(null,"Board is too small");
					}
					else{
					//	JOptionPane.showMessageDialog(null,"IN GLIDER");
						set.add(new Coords2D(center_x,center_y+1));
						set.add(new Coords2D(center_x - 1,center_y + 1));
						set.add(new Coords2D(center_x + 1,center_y +1 ));
						set.add(new Coords2D(center_x + 1,center_y ));
						set.add(new Coords2D(center_x,center_y - 1));
					}
					break;
				case "Glider Gun":
					if(w<38 || h<11){
						JOptionPane.showMessageDialog(null,"Board is too small");
					}
					else{
						//JOptionPane.showMessageDialog(null,"IN GLIDER GUN");
						set.add(new Coords2D(center_x,center_y));
						set.add(new Coords2D(center_x-1,center_y));
						set.add(new Coords2D(center_x-1,center_y-1));
						set.add(new Coords2D(center_x-1,center_y+1));
						set.add(new Coords2D(center_x-2,center_y-2));
						set.add(new Coords2D(center_x-2,center_y+2));
						set.add(new Coords2D(center_x-3,center_y));
						set.add(new Coords2D(center_x-4,center_y+3));
						set.add(new Coords2D(center_x-4,center_y-3));
						set.add(new Coords2D(center_x-5,center_y+3));
						set.add(new Coords2D(center_x-5,center_y-3));
						set.add(new Coords2D(center_x-6,center_y+2));
						set.add(new Coords2D(center_x-6,center_y-2));
						set.add(new Coords2D(center_x-7,center_y));
						set.add(new Coords2D(center_x-7,center_y-1));
						set.add(new Coords2D(center_x-7,center_y+1));
						set.add(new Coords2D(center_x-16,center_y));
						set.add(new Coords2D(center_x-16,center_y-1));
						set.add(new Coords2D(center_x-17,center_y));
						set.add(new Coords2D(center_x-17,center_y-1));
						set.add(new Coords2D(center_x+3,center_y-1));
						set.add(new Coords2D(center_x+3,center_y-2));
						set.add(new Coords2D(center_x+3,center_y-3));
						set.add(new Coords2D(center_x+4,center_y-1));
						set.add(new Coords2D(center_x+4,center_y-2));
						set.add(new Coords2D(center_x+4,center_y-3));
						set.add(new Coords2D(center_x+5,center_y));
						set.add(new Coords2D(center_x+5,center_y-4));
						set.add(new Coords2D(center_x+7,center_y));
						set.add(new Coords2D(center_x+7,center_y+1));
						set.add(new Coords2D(center_x+7,center_y-4));
						set.add(new Coords2D(center_x+7,center_y-5));
						set.add(new Coords2D(center_x+17,center_y-2));
						set.add(new Coords2D(center_x+17,center_y-3));
						set.add(new Coords2D(center_x+18,center_y-2));
						set.add(new Coords2D(center_x+18,center_y-3));
					}
					break;
				default:
					break;						
				}
				if(StartWindow.startData.getVariant()=="quad life")
					addSetToTab(set,GameStatesFactory.getStates(game).get(QuadState.BLUE));
				else
					addSetToTab(set,GameStatesFactory.getStates(game).get(BinaryState.ALIVE));
			}
		};
	}
	private static void addSetToTab(Set<Coords2D> structure,Color toSet){
		int tabPos;
		for(Coords2D coord : structure){
			tabPos=coord.x  + (coord.y * w);
		//	JOptionPane.showMessageDialog(null,"x(" + coord.x + ") y("+coord.y + ") = tabPos (" + tabPos + ")");
			
			tab[tabPos].setBackground(toSet);
		}
	}
	
	
}
