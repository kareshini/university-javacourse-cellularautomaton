package Controler;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;

import Model.AntState;
import Model.Automaton;
import Model.CellCoordinates;
import Model.CellState;
import Model.Coords1D;
import Model.Coords2D;
import Model.LangtonCell;
import View.GameStatesFactory;
import View.StartWindow;
/**
 * klasa wykonujaca zadanie co jakis okres czasu
 * @author Dominik Katszer
 *
 */
public class TimerTask {
	private Automaton prev ;
	/**
	 * poczatek naliczania czasu
	 */
	public static long beginTime;
	private WindowControler wc;
	private long elapsedTime=0;
	private boolean init=false;
	/**
	 * konstruktor
	 * @param initial zerowy stan gry
	 * @param _wc kontoler oknami
	 */
	public TimerTask(Automaton initial,WindowControler _wc){
		prev = initial;
		wc=_wc;
	}
	/**
	 * odpalenie akcji czasowej
	 */
	public void run() {
		if(!init){
			init=true;
			if(StartWindow.startData.getGame().equals("Langton Ant")){
				Map<CellCoordinates,CellState> map = changeTabToMap(wc.getTab());
				//map is ok
				int i=0;
				for(CellState el:map.values()){
					LangtonCell lc =(LangtonCell)el;
					if(lc.antState!=AntState.NONE){
						lc.antId=i;
						i++;
					}
				}
				prev.insertStructure(map);
			}
			else
				prev.insertStructure(changeTabToMap(wc.getTab()));
		}
		elapsedTime =System.currentTimeMillis() - beginTime; 
		if(wc.getFPM()==0);
		else if(elapsedTime >= 60000/wc.getFPM()){
			beginTime = System.currentTimeMillis();
			/*
			System.out.println("***************");
			for(CellCoordinates el : prev.getMap().keySet()){
				System.out.println(el + " : " + prev.getMap().get(el));
			}
			*/
			Automaton newAut = prev.nextState();
			
			//System.out.println("TimerTask , nextState , newAutomat("+newAut+")");
			/*System.out.println("***************");
			for(CellCoordinates el : newAut.getMap().keySet()){
				System.out.println(el + " : " + newAut.getMap().get(el));
			}*/
		//	System.exit(0); 
			
			
			//newAut is ok
			wc.changeBoardColor(newAut);
			//System.out.println("After changing colors to nextState");
			prev=newAut;
		}	
			
	}
	
	private Map<CellCoordinates,CellState> changeTabToMap(JButton[] tab){
		Map<CellCoordinates,CellState> map = new HashMap<CellCoordinates,CellState>();
		
		for(int i=0 ; i<tab.length ;i++){
			JButton el = tab[i];
			for(Entry<CellState, Color> e :GameStatesFactory.getStates(StartWindow.startData.getGame()).entrySet()){
				if(e.getValue().equals(el.getBackground())){
					int x=i%StartWindow.startData.getWidth();
					int y=i/StartWindow.startData.getWidth();
					if(StartWindow.startData.getDimension()!=1)
						map.put(new Coords2D(x,y), e.getKey());
					else{
						if(y==StartWindow.startData.getHeight()-1)
							map.put(new Coords1D(x), e.getKey());
					}
				}
			}
		}
		return map;
	}
}