package Controler;

import java.awt.Dimension;
/**
 * Baza danych przechowujaca informacje o wybranych opcjach z okna startowego.
 * @author Dominik Katszer
 *
 */
public class StartDataBase {
	private String 
		game,
		variant;
	private Dimension bs ;
	private boolean wrapping ;
	private boolean neighborhood;//true = moore
	private int radius;
	private int dimension;
	
	//************SET***************
	/**
	 * setter
	 * @param Game nazwa gry
	 * @param Variant nazwa variantu
	 */
	public void setGame(String Game , String Variant){
		this.game=Game;
		this.variant=Variant;
		if(Game.equals("Rule")){
			dimension=1;
		}
		else
			dimension=2;
	}
	/**
	 * setter
	 * @param neighbors true = moore
	 * @param rad promien sasiedztwa
	 */
	public void setNeighborhood(boolean neighbors,int rad){
		neighborhood = neighbors;
		radius = rad;
	}
	/**
	 * setter
	 * @param b rozmiar planszy
	 */
	public void setBoard(Dimension b){
		bs=b;
	}
	/** 
	 * setter
	 * @param b ustawienie opcji zwiazanej z zawijaniem
	 */
	public void setWrapping(boolean b){
		wrapping = b;
	}
	
	//*************GET***************
	/** 
	 * getter
	 * @return nazwa wybranej gry
	 */
	public String getGame(){
		return game;
	}
	/**
	 * getter
	 * @return nazwa wybranego wariantu
	 */
	public String getVariant(){
		return variant;
	}
	/**
	 * getter
	 * @return czy sasiedztwo to moor
	 */
	public boolean isMoore(){
		return neighborhood;
	}
	/**
	 * getter
	 * @return promien sasiedztwa
	 */
	public int getRadius(){
		return radius;
	}
	/**
	 * getter
	 * @return szerokosc planszy
	 */
	public int getWidth(){
		return (int) bs.getWidth();
	}
	/**
	 * getter
	 * @return wysokosc planszy
	 */
	public int getHeight(){
		return (int) bs.getHeight();
	}
	/**
	 * getter
	 * @return czy jest zawijanie
	 */
	public boolean isWrapping(){
		return wrapping;
	}
	/**
	 * getter
	 * @return zwraca czy gra jest jednowymiarowa czy dwu
	 */
	public int getDimension(){
		return dimension;
	}
}