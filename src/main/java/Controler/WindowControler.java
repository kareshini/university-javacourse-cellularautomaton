package Controler;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Model.Automaton;
import Model.CellStateFactory;
import Model.UniformStateFactory;
import View.GameStatesFactory;
import View.MainWindow;
import View.StartWindow;
/**
 * Kontroler nad oknami
 * @author Dominik Katszer
 *
 */
public class WindowControler {
	private MainWindow mainWindow=null;
	private StartWindow startWindow = null;
	private boolean gameWindow = false;
	private CellStateFactory cellStateFactory; 
	/** 
	 * Konstruktor
	 * @param sw okno poczatkowe
	 */
	public WindowControler(StartWindow sw){
		startWindow = sw;
		setStartButton();
	}
	private void setStartButton(){
		startWindow.setStartButton(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				startWindow.saveData();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				startWindow.setVisible(false);
				mainWindow=new MainWindow(new Dimension(StartWindow.startData.getWidth(),StartWindow.startData.getHeight()));
				setExitAction();
				startWindow.dispose();
				gameWindow=true;
				//startWindow.dispatchEvent(new WindowEvent(startWindow, WindowEvent.WINDOW_CLOSING));
				
				String game=StartWindow.startData.getGame();
				cellStateFactory = new UniformStateFactory(GameStatesFactory.getStartedState(game));
				
			}
		});
	}
	private void setExitAction(){
		mainWindow.setExitAction(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				mainWindow.setVisible(false);
				startWindow = new StartWindow();
				setStartButton();
				mainWindow.dispose();
				gameWindow=false;
				MainWindow.isPlay=false;
			}
			
		});
	}
	/**
	 * Sprawdzenie ktore okno jest wlaczone
	 * @return true jesli okno gry
	 */
	public boolean isGameWindow(){
		return gameWindow;
	}
	/**
	 * getter
	 * @return obiekt inicjalizujacy
	 */
	public CellStateFactory getCellStateFactory(){
			return cellStateFactory;
	}
	/**
	 * getter
	 * @return plansza gry
	 */
	public JButton[] getTab(){
		return mainWindow.getTab();
	}
	/**
	 * getter
	 * @return frame per minute
	 */
	public int getFPM(){
		return mainWindow.getFPM();
	}
	/**
	 * zmiana kolorow na planszy
	 * @param aut nowy stan gry
	 */
	public void changeBoardColor(Automaton aut){
		mainWindow.changeBoardColors(aut);
	}
	
}
