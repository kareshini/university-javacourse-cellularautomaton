package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
/**
 * okno do wyswietlenia , klasa bazowa do okien wyswietlanych
 * @author Dominik Katszer
 *
 */
public abstract class Window extends JFrame {
	private static final long serialVersionUID = 5880987422776047353L;
	/**
	 * odpalenie okna, wyswietlenie
	 * @param dim rozmiar okna
	 * @param window dane okno do wyswietlenia
	 */
	protected <T extends Window> void runWindow(Dimension dim,T window){
		SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
				window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
				window.setSize(dim);
				window.centerWindow();
				window.setVisible(true);
				
			}
			
		});
			
	}			
	/**
	 * wycentrowanie okna na monitorze
	 */
	protected void centerWindow(){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screenSize.width - this.getWidth())/2;
		int y = (screenSize.height - this.getHeight())/2;
		this.setLocation(x,y);
		//Warning ! This method has to be write after setting window size because of getWidth() and getHeight()
	}
	/**
	 * ustawienie obramowki z  tytulem
	 * @param title tytul
	 * @return obramowka z tytulem 
	 */
	protected TitledBorder readyBorder(String title){
		TitledBorder border = new TitledBorder(new LineBorder(Color.gray,3),title);
		border.setTitleJustification(TitledBorder.CENTER);
		return border;
	}
}
