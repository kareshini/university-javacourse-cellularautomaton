package View;

import java.awt.Component;
import java.awt.LayoutManager;

import javax.swing.JPanel;
import javax.swing.border.Border;
/**
 * klasa delegacyjna Jpanelu w celu ladniejszej oblsugi
 */
public abstract class PanelDelegation {
	private JPanel panel = new JPanel();
	
	public void setLayout(LayoutManager mng){
		panel.setLayout(mng);
	}
	public void setBorder(Border border){
		panel.setBorder(border);
	}
	public void add(Component comp){
		panel.add(comp);
	}
	protected void remove(Component comp){
		panel.remove(comp);
	}
	public JPanel getPanel(){
		return panel;
	} 
}
