package View;

import java.awt.Dimension;

import javax.swing.JButton;
/**
 * plansza gry
 * @author Dominik Katszer
 *
 */
public class Board {
	private JButton[] tabOfButtons = null;
	private Dimension sizeOfBox = null;

	/**
	 * konstruktor
	 * @param boardSize rozmiar gry 
	 * @param initBox rozmiar miejsca przeznaczonego na plansze
	 * @param menuWidth szerokosc paska menu
	 */
	public Board(Dimension boardSize, Dimension initBox , int menuWidth){	
		int size = (int) (boardSize.getHeight()*boardSize.getWidth());
		tabOfButtons = new JButton[size];
		
		sizeOfBox = new Dimension((int) ((int)(initBox.getWidth()-menuWidth)/boardSize.getWidth()),(int)(initBox.getHeight()/boardSize.getHeight()));

		for(int i=0 ; i<size ; i++){ //tabOfButtons init
			tabOfButtons[i] = new JButton();
			tabOfButtons[i].setPreferredSize(sizeOfBox);
		}
	}
	/**
	 * ustawienie edytowalnosci planszy
	 * @param isEnable edytowalne czy nie
	 */
	public void setEnabilityBoard(boolean isEnable){
		for(JButton btn : tabOfButtons){
			btn.setEnabled(isEnable);
		}
	}
	/**
	 * getter 
	 * @return tablice buttonow reprezentujaca plansze
	 */
	public JButton[] getTab(){
		return tabOfButtons;
	}

}
