package View;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import Controler.StartDataBase;
import StartPanel.ButtonsStartPanel;
import StartPanel.DimensionStartPanel;
import StartPanel.GameStartPanel;
import StartPanel.NeighborhoodStartPanel;
import StartPanel.WrappingStartPanel;
/**
 * Pierwsze okno menu
 * @author Dominik Katszer
 *
 */
public class StartWindow extends Window {
	
	//Attributes
	private static final long serialVersionUID = -2202820661321380370L;
	
	private GameStartPanel game = null;
	private NeighborhoodStartPanel neighborhood=new NeighborhoodStartPanel();
	private DimensionStartPanel board=null;
	private WrappingStartPanel wrapping=new WrappingStartPanel();
	private ButtonsStartPanel buttons=new ButtonsStartPanel();
	/**
	 * klasa zapisujaca dane , opcje wybrane w tym oknie
	 */
	public static StartDataBase startData = new StartDataBase();
	
	private final int initWidth = 300;
	private final int initHeight = 500;
	//*******constructor****
	/**
	 * konstruktor
	 */
	public StartWindow(){
		this.setTitle("CallularAutomatons - Menu");
		uiGame();
		uiBoard();
		uiButtons();
		uiNeighborhood();
		uiWrapping();
		
		
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill=GridBagConstraints.BOTH;//HORIZNTAL
		
		c.ipadx=40;
		
		c.gridx=0;
		c.gridy=0;
		this.add(game.getPanel(),c);
		c.gridy=1;
		this.add(board.getPanel(),c);
		c.ipady=0;
		c.gridy=2;
		this.add(neighborhood.getPanel(), c);
		c.gridy=3;
		this.add(wrapping.getPanel(),c);
		c.gridy=4;
		this.add(buttons.getPanel(), c);
		
		runWindow(new Dimension(this.initWidth,this.initHeight),this);
		//every event should be passed to SwingUtilities.invokeLAter();
	}
	
	//********ui****************
	private void uiGame(){
		String[] gameListString = {"Game of Live" , "Langton Ant" , "Wireworld" , "Rule"};
		game = new GameStartPanel(gameListString);
		game.setLayout(new GridLayout(2, 1));
		game.setBorder(readyBorder("Game"));	
	}
	private void uiNeighborhood(){
		neighborhood.setLayout(new GridLayout(2, 2));	
		neighborhood.setBorder(this.readyBorder("Neighborhood"));
	}
	private void uiBoard(){
		board=new DimensionStartPanel();
		board.setLayout(new GridLayout(4,1));
		board.setBorder(this.readyBorder("Board Size"));
	}
	private void uiWrapping(){
		
		wrapping.setBorder(this.readyBorder("Wrapping"));
	}
	private void uiButtons(){	
		buttons.setBorder(this.readyBorder("What next ?"));
	}
	/**
	 * ustawianie akcji dla przycisku start
	 * @param l akcja
	 */
	public void setStartButton(ActionListener l){
		buttons.setStartButton(l);
	}
	/**
	 * Zapisanie wybranych ustawien
	 */
	public void saveData(){
		StartWindow.startData.setGame(game.getInfo()[0],game.getInfo()[1]);
		StartWindow.startData.setNeighborhood(neighborhood.isMoore(),neighborhood.getRadius());
		StartWindow.startData.setBoard(board.getInfo());
		StartWindow.startData.setWrapping(wrapping.getInfo());
	}
	//**********main*************
	public static void main(String[] args){
		new StartWindow();
	}
}	
