package View;
/**
 * Fabryka wariantow
 * @author Dominik Katszer
 *
 */
public class VariantsFactory {
	/**
	 * Getter variantow
	 * @param game nazwa gry
	 * @return Tablica nazw wariantow
	 */
	static public String[] getVariants(String game){
		String[] tab;
		if(game=="Game of Live"){
			tab = new String[]{"23/3" , "quad life" , "insert variant" ,};
		}
		else if(game=="Langton Ant"){
			tab = new String[]{"Just one variant of game."};
		}
		else if(game=="Wireworld"){
			tab = new String[]{"Just one variant of game."};
		}
		else if(game=="Rule"){
			tab = new String[] {"30" , "110" , "insert variant"};			
		}
		else{
			tab = new String[]{""};
		}
		
		return tab;
	}
}
