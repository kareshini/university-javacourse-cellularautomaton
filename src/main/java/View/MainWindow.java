package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JPanel;

import Controler.ActionListenerStructures;
import Controler.TimerTask;
import MainPanel.FuncMainPanel;
import MainPanel.InitMainPanel;
import MainPanel.SpeedMainPanel;
import Model.Automaton;
import Model.Automaton1Dim;
import Model.Automaton2Dim;
import Model.CellState;
import Model.Coords1D;
import Model.Coords2D;
/**
 * Glowne okno gry 
 * @author Dominik Katszer
 *
 */
public class MainWindow extends Window {

	//Attributes
	private static final long serialVersionUID = -8327573611079109005L;
	
	private JPanel board = new JPanel();
	private JPanel menu = new JPanel();
	
	private SpeedMainPanel speed =new SpeedMainPanel();
	private FuncMainPanel func = new FuncMainPanel();
	private InitMainPanel init  ;
	
	private Board gameBoard =null;
	/**
	 * czy gra nie jest zastopowana
	 */
	public static boolean isPlay=false;
	
	private final int menuWidth =230;
	private final Dimension initBox = new Dimension(700,500);
	/**
	 * czy okno glowne gry jest wlaczone 
	 */
	public static boolean play = false;
	
	//*******constructor****
	/**
	 * konstruktor
	 * @param boardSize rozmiar planszy
	 */
	public MainWindow(Dimension boardSize){
		Set<CellState> states = GameStatesFactory.getStates(StartWindow.startData.getGame()).keySet();
		Set<String> structures = GameStructuresFactory.getStructures(StartWindow.startData.getGame());
		
		uiBoard(boardSize);
		
		init = new InitMainPanel(states.toArray(new CellState[states.size()]),structures.toArray(new String[structures.size()]),gameBoard.getTab());
		
		this.setTitle("CallularAutomatons - Game");
		uiMenu();
		
		
		
		this.add(menu,BorderLayout.WEST);
		this.add(board, BorderLayout.CENTER);
		
		setStartAction();
		setStopAction();
		
		this.runWindow(initBox, this);
		
	}
	//********ui****************}
	private void uiMenu(){
		speed.setLayout(new GridLayout(2, 1));
		speed.setBorder(readyBorder("Speed"));
		func.setLayout(new GridLayout(3, 1));
		func.setBorder(readyBorder("Usefull buttons"));
		init.setLayout(new GridLayout(4, 1));		
		init.setBorder(readyBorder("Initialization"));
		
		//-------------------------------
		
		menu.setLayout(new GridLayout(3,1));
		menu.setPreferredSize(new Dimension(menuWidth,this.getHeight()));

		menu.add(speed.getPanel());
		menu.add(func.getPanel());
		menu.add(init.getPanel());
		
		menu.setBorder(this.readyBorder("Menu"));
	
	}
	private void uiBoard(Dimension boardSize){
		gameBoard = new Board(boardSize, initBox, menuWidth);
		if(StartWindow.startData.getDimension()==2){
			for(JButton el:gameBoard.getTab()){
				el.setBackground(GameStatesFactory.getStates(StartWindow.startData.getGame()).get(GameStatesFactory.getStartedState(StartWindow.startData.getGame())));
				el.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e) {//*********************************Plansza
						el.setBackground(GameStatesFactory.getStates(StartWindow.startData.getGame()).get(init.getChosenState()));
					}
					
				});
				board.add(el);
			}
		}
		else{
			int counter = 0;
			for(JButton el : gameBoard.getTab()){
			//	System.out.println(counter);
				el.setBackground(GameStatesFactory.getStates(StartWindow.startData.getGame()).get(GameStatesFactory.getStartedState(StartWindow.startData.getGame())));
				if(counter>boardSize.width * (boardSize.height-1) - 1 ){ //dolne
					el.addActionListener(new ActionListener(){
						@Override
						public void actionPerformed(ActionEvent e) {//*********************************Plansza
							el.setBackground(GameStatesFactory.getStates(StartWindow.startData.getGame()).get(init.getChosenState()));
						}
						
					});
				}
				else{
					el.setEnabled(false);
				}
				counter++;
				board.add(el);
			}
		}
		
		board.setLayout(new GridLayout((int)boardSize.getHeight(),(int)boardSize.getWidth()));
		board.setPreferredSize(new Dimension(this.getWidth()-menuWidth,this.getHeight()));

		board.setBorder(readyBorder("Game"));
	}
	private void setStartAction(){
		func.setStartAction(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				TimerTask.beginTime=System.currentTimeMillis();
				isPlay=true;
				setEnabilityBoard(false);
			//	System.out.println("StartButton has pressed  +  isPlay("+isPlay+") <-should be true");
					
			}
			
		});
	}
	private void setStopAction(){
		func.setStopAction(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				isPlay=false;			
			}
				
		});		
	}
	/**
	 * ustawienie akcji exit
	 * @param listener akcja
	 */
	public void setExitAction(ActionListener listener){
		func.setExitAction(listener);
	}
	
	private void setEnabilityBoard(boolean isEnable){
//		System.out.println("MainWindow setEnabilityBoard("+isEnable+")");
		gameBoard.setEnabilityBoard(isEnable);
	}
	/**
	 * getter 
	 * @return tablica buttonow reprezentujaca plansze 
	 */
	public JButton[] getTab(){
		return gameBoard.getTab();
	}
	
	/**
	 * Zmiana kolorow buttonow na planszy
	 * @param aut dany stan gry 
	 */
	public void changeBoardColors(Automaton aut ){
		int tabPos;
	//	System.out.println("MainWindow changeBoardColors arg("+aut+")");
		if(aut instanceof Automaton2Dim){
	//		System.out.println("MainWindow changeBoardColors Automaton2Dim");
			for(Model.Cell cell : aut){
				tabPos=((Coords2D)cell.coords).y * ((Automaton2Dim) aut).getWidth() + ((Coords2D)cell.coords).x;
		//		System.out.println("MainWindow changeBoardColor for aut :cell.state =  "+cell.state);
				gameBoard.getTab()[tabPos].setBackground(GameStatesFactory.getStates(StartWindow.startData.getGame()).get(cell.state));//edytowac wszystko ze zamiast stringa ma byc state
		//		System.out.println("MainWindow changeBoardColors Automaton2Dim , " + gameBoard.getTab()[tabPos] + " :: " + GameStatesFactory.getStates(StartWindow.startData.getGame()).get(cell.state));
			}
			
		}
		else if(aut instanceof Automaton1Dim){
			/*if(aut==null)     aut nie jest null
				System.exit(0);*/
			/*for(int i =0 ; i < 10 ; i++){
				if(aut.getMap().get(new Coords1D(i)).equals(BinaryState.ALIVE))
					System.exit(0);// znajdywanie zywego
			}*/

			upShift(StartWindow.startData.getWidth());
			changeLastLine(aut,StartWindow.startData.getWidth());
		}
			
	}
	private void upShift(Integer width){
		for(int i=0 ; i<gameBoard.getTab().length - width ; i++){
			gameBoard.getTab()[i].setBackground(gameBoard.getTab()[i+width].getBackground());
			gameBoard.getTab()[i+width].setBackground(GameStatesFactory.getStates(StartWindow.startData.getGame()).get(GameStatesFactory.getStartedState(StartWindow.startData.getGame())));
		}
	}
	private void changeLastLine( Automaton aut , int width){
		int lastLine = StartWindow.startData.getWidth() * (StartWindow.startData.getHeight()-1);
		int tabPos;

		for(int i =0 ; i <width ; i++){
			tabPos=i + lastLine;
			gameBoard.getTab()[tabPos].setBackground(GameStatesFactory.getStates(StartWindow.startData.getGame()).get(aut.getMap().get(new Coords1D(i))));//edytowac wszystko ze zamiast stringa ma byc state
		}
	}
	/**
	 * getter
	 * @return frame per minute
	 */
	public int getFPM(){
		return speed.getSpeed();
	}
	//**********main*************
	public static void main(String[] args){
		new MainWindow(new Dimension(40,40));
	}
	
	
}
