package View;

import java.util.HashSet;
import java.util.Set;
/**
 * Fabryka struktur
 * @author Dominik Katszer
 *
 */
public class GameStructuresFactory {
	private static Set<String> GoL = new HashSet<String>();//add Structure
	private static Set<String> WireWorld = new HashSet<String>();//add Structure
	private static Set<String> Rule = new HashSet<String>();//add Structure
	private static boolean initialized = false;
	private static void init(){
		GoL.add("None");
		GoL.add("Block");
		GoL.add("Blinker");
		GoL.add("Glider");
		GoL.add("Glider Gun");
		
		WireWorld.add("None");
//		WireWorld.add("Diode");
		
		
		Rule.add("None");
	}
	/**
	 * getter struktur
	 * @param game nazwa gry
	 * @return zwior nazw struktur dostepnych
	 */
	public static Set<String> getStructures(String game){
		if(!initialized){
			init();
			initialized=true;
		}
		if(game=="Game of Live"){	
			return GoL;
		}
		else if(game=="Langton Ant"){
			return Rule;
		}
		else if(game=="Wireworld"){
			return WireWorld;
		}
		else if(game=="Rule"){		
			return Rule;
		}
		
		return null;
	}
}
