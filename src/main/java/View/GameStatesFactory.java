package View;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import Model.AntState;
import Model.BinaryState;
import Model.CellState;
import Model.LangtonCell;
import Model.QuadState;
import Model.WireElectronState;
/**
 * Fabryka stanow i kolorow im odpowiadajacym
 * @author Dominik Katszer
 *
 */
public class GameStatesFactory {
	private static Map<CellState,Color> GoL = new HashMap<CellState,Color>();
	private static Map<CellState,Color> QuadLife = new HashMap<CellState,Color>();
	private static Map<CellState,Color> Wireworld = new HashMap<CellState,Color>();
	private static Map<CellState,Color> LangtonAnt = new HashMap<CellState,Color>();
	
	private static boolean initialized = false;
	//private static boolean onlyColor=false;
	private static void init(){
		if(!initialized){
			initialized=true;
			GoL.put(BinaryState.ALIVE, Color.BLACK);
			GoL.put(BinaryState.DEAD, Color.WHITE);
						
			Wireworld.put(WireElectronState.VOID, Color.BLACK);
			Wireworld.put(WireElectronState.WIRE, Color.YELLOW);
			Wireworld.put(WireElectronState.ELECTRON_HEAD, Color.BLUE);
			Wireworld.put(WireElectronState.ELECTRON_TAIL, Color.RED);
			
			QuadLife.put(QuadState.DEAD, Color.WHITE);
			QuadLife.put(QuadState.BLUE, Color.BLUE);
			QuadLife.put(QuadState.GREEN, Color.GREEN);
			QuadLife.put(QuadState.RED, Color.RED);
			QuadLife.put(QuadState.YELLOW, Color.YELLOW);
			
			
			LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.NONE), Color.WHITE);
			
			LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.NORTH), Color.RED);
			LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.EAST), Color.RED);
			LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.SOUTH), Color.RED);
			LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.WEST), Color.RED);
			
			
		}
	}
	/**
	 * getter stanow i ich kolorow
	 * @param game nazwa gry
	 * @return mapa stanow komorek do kolorow
	 */
	public static Map<CellState,Color> getStates(String game){
		init();

		if(game=="Game of Live"){	
			if(StartWindow.startData.getVariant().equals("quad life"))
				return QuadLife;
			else
				return GoL;
		}
		else if(game=="Langton Ant"){
			if(MainWindow.isPlay){
				LangtonAnt.put(new LangtonCell(BinaryState.ALIVE, null, AntState.NONE), Color.BLACK);
				
				LangtonAnt.put(new LangtonCell(BinaryState.ALIVE, null, AntState.NORTH), Color.RED);
				LangtonAnt.put(new LangtonCell(BinaryState.ALIVE, null, AntState.EAST), Color.RED);
				LangtonAnt.put(new LangtonCell(BinaryState.ALIVE, null, AntState.SOUTH), Color.RED);
				LangtonAnt.put(new LangtonCell(BinaryState.ALIVE, null, AntState.WEST), Color.RED);
			}
			else
			{
				LangtonAnt.clear();
				LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.NONE), Color.WHITE);
				
				LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.NORTH), Color.RED);
				LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.EAST), Color.RED);
				LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.SOUTH), Color.RED);
				LangtonAnt.put(new LangtonCell(BinaryState.DEAD, null, AntState.WEST), Color.RED);
			}
			return LangtonAnt;
		}
		else if(game=="Wireworld"){
			return Wireworld;
		}
		else if(game=="Rule"){	
			return GoL;
		}
		
		return null;
	}
	/**
	 * Zwrocenie stanu inicjalizacyjnego
	 * @param game nazwa gry
	 * @return stan inicjalizacyjny
	 */
	public static CellState getStartedState(String game) {
		if(game=="Game of Live"){	
			if(StartWindow.startData.getVariant().equals("quad life"))
				return QuadState.DEAD;
			else
				return BinaryState.DEAD;
		}
		else if(game=="Langton Ant"){
			return new LangtonCell(BinaryState.DEAD, null, AntState.NONE);
		}
		else if(game=="Wireworld"){
			return WireElectronState.VOID;
		}
		else if(game=="Rule"){
			return BinaryState.DEAD;
		}
		return null;
	}
}
