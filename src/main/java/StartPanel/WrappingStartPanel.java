package StartPanel;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import View.PanelDelegation;
/**
 * Panel zwiazany z wyborem opcji wrappingu
 * @author Dominik Katszer
 *
 */
public class WrappingStartPanel extends PanelDelegation{
	private JRadioButton b1 = new JRadioButton("No",true);
	private JRadioButton b2 = new JRadioButton("Yes");
	/**
	 * Konstruktor
	 */
	public WrappingStartPanel(){
		ButtonGroup group = new ButtonGroup();
		group.add(b1);
		group.add(b2);
		
		add(b1);
		add(b2);		
	}
	/**
	 * Informacja o wrappingu
	 * @return true or false w zaleznosci czy ustawiony jest wrapping
	 */
	public boolean getInfo(){
		if(b1.isSelected())
			return false;
		else
			return true;
	}
}
