package StartPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import View.PanelDelegation;
/**
 * Panel z buttonami
 * @author Dominik Katszer
 *
 */
public class ButtonsStartPanel extends PanelDelegation {
	private JButton b1 = new JButton("Exit");
	private JButton b2 = new JButton("Start");
	/**
	 * Konstruktor
	 */
	public ButtonsStartPanel(){
		b1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}		
		});
		
		add(b1);
		add(b2);
	}
	/**
	 * Ustawiaa akcje przycisku start
	 * @param listener akcja (nasluchiwacz)
	 */
	public void setStartButton(ActionListener listener){
		remove(b2);
		b2.addActionListener(listener);
		add(b2);
	}
}
