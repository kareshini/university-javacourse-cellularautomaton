package StartPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import View.PanelDelegation;
import View.VariantsFactory;
/**
 * Panel zwiazany z wyborem gry i wariantu
 * @author Dominik Katszer
 *
 */
public class GameStartPanel extends PanelDelegation {
	private String[] listOfGames;
	private JComboBox<String> gameList = null;
	private JComboBox<String> variant= null;
	/**
	 * konstruktor
	 * @param gameListString Lista gier dostepnych
	 */
	public GameStartPanel(String[] gameListString){
		listOfGames=gameListString;
		gameList = new JComboBox<String>(listOfGames);
		variant= new JComboBox<String>();//empty
		
		gameList.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				variant.removeAllItems();
				for(String el : VariantsFactory.getVariants((String) (gameList.getSelectedItem())))
					variant.addItem(el);
			}	
		});
		
		gameList.setSelectedIndex(0);
		variant.setSelectedIndex(0);
		
		variant.addActionListener(new ActionListener(){
//"insert variant"
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int lastIdx=variant.getItemCount()-1;
				if((String)variant.getSelectedItem()=="insert variant"){
					String var= JOptionPane.showInputDialog("Enter variant (number/number) : ");
					if(var==null){
						variant.setSelectedIndex(0);
					}
					else{
						if((Pattern.matches("\\d+/\\d+", var)&&((String)gameList.getSelectedItem()).equals("Game of Live"))){
							String last = variant.getItemAt(lastIdx);
							variant.removeItemAt(lastIdx);
							variant.addItem(var);
							variant.addItem(last);
							variant.setSelectedIndex(lastIdx);
						}
						else if(Pattern.matches("\\d+", var)&&((String)gameList.getSelectedItem()).equals("Rule")){
							if(Integer.parseInt(var)>255 ||Integer.parseInt(var)<0){
								JOptionPane.showMessageDialog(null, "Wrong variant input");
								variant.setSelectedIndex(0);
							}
							else{
								String last = variant.getItemAt(lastIdx);
								variant.removeItemAt(lastIdx);
								variant.addItem(var);
								variant.addItem(last);
								variant.setSelectedIndex(lastIdx);
							}
						}
						else{
							JOptionPane.showMessageDialog(null, "Wrong variant input");
							variant.setSelectedIndex(0);
						}
					}
				}
			}
			
		});
		
		add(gameList);
		add(variant);
	}
	/**
	 * getter o wybranych opcjahc
	 * @return Tablica stringow reprezentujaca wybrana gre oraz wybrany variant
	 */
	public String[] getInfo(){
		String[] tmp ={(String)gameList.getSelectedItem(),(String)variant.getSelectedItem()};
		return tmp;
	}
	
}
