package StartPanel;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

import View.PanelDelegation;
/**
 * Panel zwiazany z wyborem rozmiaru planszy
 * @author Dominik Katszer
 *
 */
public class DimensionStartPanel extends PanelDelegation{
	private static final int
		INIT_SIZE_MIN=0,
		INIT_SIZE_MAX=100,
		INIT_SIZE_INIT=10;
	private JSlider width;
	private JSlider height;
	private JLabel widthLabel=new JLabel("Width",SwingConstants.CENTER);
	private JLabel heightLabel=new JLabel("Height",SwingConstants.CENTER);
	
	private void setSlider(JSlider slider) {
		slider.setMajorTickSpacing(20);
		slider.setMinorTickSpacing(5);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setSnapToTicks(true);
	}
	/**
	 * Konstruktor
	 * @param SIZE_MIN minimalny rozmiar gry
	 * @param SIZE_MAX maksymalny rozmiar gry
	 * @param SIZE_INIT jaki jest domyslny rozmiar gry
	 */
	public DimensionStartPanel(int SIZE_MIN,int SIZE_MAX,int SIZE_INIT){
		width = new JSlider(JSlider.HORIZONTAL,SIZE_MIN,SIZE_MAX,SIZE_INIT);
		height = new JSlider(JSlider.HORIZONTAL,SIZE_MIN,SIZE_MAX,SIZE_INIT);
		
		setSlider(width);
		setSlider(height);
		
		add(widthLabel);
		add(width);
		add(heightLabel);
		add(height);
	}
	/**
	 * Konstruyktor domyslny
	 */
	public DimensionStartPanel(){
		this(DimensionStartPanel.INIT_SIZE_MIN,DimensionStartPanel.INIT_SIZE_MAX,DimensionStartPanel.INIT_SIZE_INIT);
	}
	/**
	 * Getter odnosnie rozmiaru gry
	 * @return razmiar gry
	 */
	public Dimension getInfo(){
		return new Dimension(width.getValue(),height.getValue());
	}
}
