package StartPanel;

import javax.swing.ButtonGroup;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;

import View.PanelDelegation;
/**
 * Panel zwiazany z wyborem sasiedztwa
 * @author Dominik Katszer
 *
 */
public class NeighborhoodStartPanel extends PanelDelegation{
	private JRadioButton b1 = new JRadioButton("Moore",true);
	private JRadioButton b2 = new JRadioButton("von Neuman");
	private JFormattedTextField radius = new JFormattedTextField(new Integer(1));
	private JLabel label = new JLabel("Radius :");
	/**
	 * Konstruktor
	 */
	public  NeighborhoodStartPanel(){
		ButtonGroup group = new ButtonGroup();
		group.add(b1);
		group.add(b2);
		
		add(b1);
		add(b2);
		add(label);
		add(radius);		
	}
	/**
	 * Sprawdzenie czy wybrane sasiedztwo to moor
	 * @return true or false w zaleznosci czy moor czy nie
	 */
	public boolean isMoore(){
		if(b1.isSelected())
			return true;
		else
			return false;
	}
	/**
	 * getter
	 * @return promien sasiedztwa
	 */
	public int getRadius(){
		return (int)radius.getValue();
	}
	/**
	 * Niepotrzebne , ustawienie niemozliwosci zmian, zastapione innym sposobem
	 * @param enable czy mozliwe edytowanie czy nie
	 */
	public void setEnability(boolean enable){
		b1.setEnabled(enable);
		b2.setEnabled(enable);
	}

}
