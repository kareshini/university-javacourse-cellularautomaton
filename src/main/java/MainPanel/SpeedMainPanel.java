package MainPanel;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;

import View.PanelDelegation;
/**
 * panel zwiazany z predkoscia gry
 * @author Dominik Katszer
 *
 */
public class SpeedMainPanel extends PanelDelegation {
	private static final int
		INIT_FPM_MIN =0,
		INIT_FPM_MAX=600,
		INIT_FPM_INIT=100;
	private JSlider fpm=null;
	private JLabel label = new JLabel("Frame per minute",SwingConstants.CENTER);
	/**
	 * Konstruktor
	 * @param FPM_MIN frame per minute minimalne
	 * @param FPM_MAX frame per minute maksymalne
	 * @param FPM_INIT frame per minute domyslne
	 */
	public SpeedMainPanel(int FPM_MIN , int FPM_MAX, int FPM_INIT){
		 fpm = new JSlider(JSlider.HORIZONTAL,FPM_MIN,FPM_MAX,FPM_INIT);
		 
		 fpm.setMajorTickSpacing(100);
		 fpm.setMinorTickSpacing(20);
		 fpm.setPaintTicks(true);
		 fpm.setPaintLabels(true);
		 fpm.setSnapToTicks(true);
		 
		 add(label);
		 add(fpm);
	}
	/**
	 * konstruktor domyslny
	 */
	public SpeedMainPanel(){
		this(INIT_FPM_MIN,INIT_FPM_MAX,INIT_FPM_INIT);
	}
	/**
	 * Getter
	 * @return pobranie predkosci ilosci frame per minute
	 */
	public int getSpeed(){
		return fpm.getValue();
	}
}
