package MainPanel;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import View.PanelDelegation;
/**
 * Panel zwiazany z przyciskami
 * @author Dominik Katszer
 *
 */
public class FuncMainPanel extends PanelDelegation {
	private JButton start = new JButton("Start");
	private JButton stop = new JButton("Stop");
	private JButton exit = new JButton("Exit");
	/**
	 * konstruktor
	 */
	public FuncMainPanel(){
		add(start);
		add(stop);
		add(exit);
	}
	/**
	 * ustawienie przycisku start
	 * @param listener akcja
	 */
	public void setStartAction(ActionListener listener){
		start.addActionListener(listener);
	}
	/**
	 * ustawienie przycisku stop
	 * @param listener akcja
	 */
	public void setStopAction(ActionListener listener){
		stop.addActionListener(listener);
	}
	/**
	 * ustawienie przycisku exit
	 * @param listener akcja
	 */
	public void setExitAction(ActionListener listener){
		exit.addActionListener(listener);
	}
}
