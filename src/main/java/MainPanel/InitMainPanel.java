package MainPanel;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

import Controler.ActionListenerStructures;
import Model.CellState;
import View.PanelDelegation;
import View.StartWindow;

/**
 * Panel zwiazany z wypelnianiem planszy albo odpowiendim stanem albo struktura
 * @author Dominik Katszer
 *
 */
public class InitMainPanel extends PanelDelegation {
	private JComboBox<CellState> state = new JComboBox<CellState>();
	private JComboBox<String> structure = new JComboBox<String>();
	private JLabel stateLabel = new JLabel("State");
	private JLabel structureLabel = new JLabel("Structure");
	/**
	 * Konstruktor
	 * @param listOfStates lista mozliwych stanow wypelnianych przy kliknieciu
	 * @param listOfStructures lista dostepnych wbudowanych struktur
	 * @param tab tablica buttonow reprezentujaca mape gry
	 */
	public InitMainPanel(CellState[] listOfStates , String[] listOfStructures ,JButton[] tab){
		for(CellState el : listOfStates){
			state.addItem(el);
		}
		for(String el : listOfStructures){
			structure.addItem(el);
		}
		structure.setSelectedItem("None");
		structure.addActionListener(ActionListenerStructures.getActionListener(structure, tab));
		
		add(stateLabel);
		add(state);
		add(structureLabel);
		add(structure);
	}
	/**
	 * getter
	 * @return wybrany stan
	 */
	public CellState getChosenState(){
		return (CellState) state.getSelectedItem();
	}
	/**
	 * getter
	 * @return wybrana struktura
	 */
	public String getChosenStructure(){
		return (String)state.getSelectedItem();
	}
	/**
	 * ustawienie akcji przy wyborze struktury
	 * @param akcja
	 */
	public void setActionListenerStructures(ActionListener l){
		structure.addActionListener(l);
	}
}
