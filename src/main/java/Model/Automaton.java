package Model;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * Klasa bazowa dla kazdego automatona komorkowego. Klasa ta przedstawia pojedynczy stan.
 * @author Dominik Katszer
 *
 */
public abstract class Automaton implements Iterable<Model.Cell> {
	//*****************Inner class*****************
	/**
	 * Prywatna klasa Iterator w celu foreach dla kazdego automatona
	 * @author Dominik Katszer
	 *
	 */
	private class CellIterator implements Iterator<Model.Cell>{
		private CellCoordinates currentState;
		public boolean hasNext(){
			return Automaton.this.hasNextCoordinates(currentState);
		}
		public Model.Cell next(){
			CellCoordinates cc = Automaton.this.nextCoordinates(currentState);
			CellState cs = Automaton.this.cells.get(cc);
			currentState = cc;//move forward
			return new Model.Cell(cc,cs);			
		}
		public void setState(CellState cs){
	//		System.out.println("Automaton iterator setState: put("+currentState+", "+ cs+")");
	//		System.out.flush();
			Automaton.this.cells.put(currentState, cs); //replace, Coords equal should be override
		}
		public CellIterator(){
			currentState=Automaton.this.initialCoordinates();
		}
	}
	//***************Attributes*************
	private Map<CellCoordinates,CellState> cells ;
	private CellNeighborhood neighborsStrategy;
	private CellStateFactory stateFactory;
	//***************Methods******************
	//privates
	private Set<Model.Cell> mapCoordinates(Set<CellCoordinates> set){
		Set<Model.Cell> tmp = new HashSet<Model.Cell>();
		for(CellCoordinates el : set){
			tmp.add(new Model.Cell(el,this.cells.get(el)));
		}
		return tmp;
	}
	//public
	/**
	 * Oblicza ona jak ma wygladac nowy stan i go zwraca .
	 * @return Zwraca nowy autmaton (czyli dana gre) reprezentujaca jej nowy stan.
	 */
	public Automaton nextState(){
	//	System.out.println("FormerCells foreach(this)1");
	//	for(Cell el: this){
	//		System.out.println(el);
	//	}
	//	System.out.println("****Automaton nextState*****");
	//	System.out.flush();
		Automaton newGame= this.newInstance(stateFactory, neighborsStrategy);
	//	System.out.println("FormerCells foreach(this)2");
	//	for(Cell el: this){
	//		System.out.println(el);
	//	}
		CellIterator it = newGame.cellIterator(); 
		
		Set<Model.Cell> newCells = this.mapCoordinates(cells.keySet());//because of !for each Cell cell in Automaton!
	//	System.out.println("FormerCells foreach(this)");
	//	for(Cell el: this){
	//		System.out.println(el);
	//	}
	//	System.out.println("newCells");
	//	for(Cell el: newCells){
	//		System.out.println(el);
	//	}
		List<Model.Cell> poKolei = new LinkedList<Model.Cell>(newCells);
		Collections.sort(poKolei);
	//	System.out.println("pokolei List");
	//	for(Cell el: poKolei){
	//		System.out.println(el);
	//	}
		for(Model.Cell c : poKolei){//newCells are hash so we dont know sequence, change to tree set but firstly we have to add comparable
			Set<CellCoordinates> neighbors = neighborsStrategy.cellNeighbors(c.coords);
			Set<Model.Cell> mappedNeighbors = mapCoordinates(neighbors); 
	//		System.out.println("***");
	//		System.out.println("Actual cell , coords("+c.coords+") , state("+c.state+")");
			CellState newState = nextCellState(c, mappedNeighbors);
			
			it.next();
	//		System.out.println("Next cell , coords("+tmp.coords+") , state("+newState+")");
			
			it.setState(newState);
			
		}
		
		return newGame;
	};
	/**
	 * Dodaje do staniu strukture
	 * @param structure jest to mapa koordynatow do stanow danej struktury.
	 */
	public void insertStructure(Map<? extends CellCoordinates, ? extends CellState> structure){
		Set<? extends CellCoordinates> keys  =structure.keySet();
		for(CellCoordinates el : keys){
			this.cells.put(el, structure.get(el)); //replace
		}
	}
	
	private CellIterator cellIterator(){
		return new CellIterator();
	}
	
	@Override
	/**
	 * Zwraca iterator aktualnego stanu
	 */
	public Iterator<Model.Cell> iterator() {
		return this.cellIterator();
	}
	//protected abstract
	/**
	 * Tworzenie nowej instancji danej gry
	 * @param cellStateFactory informacja jak ma zaiicjalizowac nowy stan.
	 * @param cellNeighborhood sasiedztwo, von neuman czy moor
	 * @return Zainicjalizowany nowy stan danej gry
	 */
	protected abstract Automaton newInstance(CellStateFactory cellStateFactory,CellNeighborhood cellNeighborhood);
	/**
	 * Sprawdzenie przez iteratora czy jest kolejny koordynat czy nie przeszedl juz calej mapy gry
	 * @param cellCoordiantes aktualny koordynat
	 * @return true or false w zaleznosci czy jest nastepny koordynat czy nie
	 */
	protected abstract boolean hasNextCoordinates(CellCoordinates cellCoordiantes);
	/**
	 * Zwrocenie koordynatu potrzebnego do ustawienia startu iteratora 
	 * @return koordynat przed poczatkiem
	 */
	protected abstract CellCoordinates initialCoordinates();
	/**
	 * Zwrocenie nastepnego koordynatu oraz przejscie do niego
	 * @param cellCoordiantes aktualny koordynat
	 * @return nastepny koordynat wzgledem argumentu
	 */
	protected abstract CellCoordinates nextCoordinates(CellCoordinates cellCoordiantes);
	/**
	 * Zwrocenie nowego stanu jaki powinna dana komorka miec przy nastepnym stanie gry.
	 * 
	 * @param currentState aktualna komorka
	 * @param neighborsStates wszyscy sasiedzi tej komorki
	 * @return Stan jaki komorka powinna przyajc
	 */
	protected abstract CellState nextCellState(Cell currentState, Set<Model.Cell> neighborsStates);
	
	/**
	 * Konstruktor
	 * @param cellStateFactory Obiekt reprezentujacy albo uniformStateFactory albo GeneralStateFactory opisujacy na jaki sposob ma zainicjalizowac automatona
	 * @param cellNeighborhood Obiekt reprezentujacy dane sasiedztwo, Moor albo von Neuman
	 */
	public Automaton(CellStateFactory cellStateFactory,CellNeighborhood cellNeighborhood ){
		neighborsStrategy = cellNeighborhood;
		stateFactory = cellStateFactory;
	}
	/**
	 * Inicjalizuje mape reprezentujaca gre argumentem
	 * @param map Mapa gry
	 */
	protected void InitCells(Map<CellCoordinates,CellState> map){
		cells= new HashMap<CellCoordinates,CellState>(map);
	}
	
	//only to test
	/**
	 * Uzywana do testow jedynie
	 * @return mapa gry tego automatona
	 */
	public Map<CellCoordinates,CellState> getMap(){
		return cells;
	}
}
