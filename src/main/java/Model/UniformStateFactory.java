package Model;

import java.util.HashMap;
import java.util.Map;
/**
 * Obiekt inicjalizujacy, inicjalizuje on wszystkie komorki na podany stan
 * @author Dominik Katszer
 *
 */
public class UniformStateFactory implements CellStateFactory {

	private CellState state;
	Map<CellCoordinates,CellState> cells = new HashMap<CellCoordinates,CellState>();
	@Override
	public void initialState(CellCoordinates cellCoords) {
		cells.put(cellCoords, state);
	}
	/**
	 * Konstruktor
	 * @param cellState stan inicjalizujacy komorki
	 */
	public UniformStateFactory(CellState cellState){
		state=cellState;
	}
	@Override
	public Map<CellCoordinates,CellState> getMap(){
		return cells;
	}

}