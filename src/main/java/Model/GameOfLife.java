package Model;

import java.util.Set;

import javax.swing.JOptionPane;

import View.StartWindow;

/**
 * Gra w zycie, automaton dwuwymiarowy . Reprezentuje pojedynczy stan tej gry
 * @author Dominik Katszer
 *
 */
public class GameOfLife extends Automaton2Dim {

	private int[] stayAlive;
	private int[] becomeLive;
	/**
	 * konstruktor
	 * @param width szerokosc mapy gry
	 * @param height wysokosc mapy gry
	 * @param cellStateFactory obiekt inicjalizujacy
	 * @param cellNeighborhood sasiedztwo
	 * @param stayAlives tablica cyfr, przechowujaca przy jakim sasiezdtwie komorka pozostaje zywa
	 * @param becomeLives tablica cyfr przechowujaca przy jakim sasiedztwie komorka ozywa
	 */
	public GameOfLife(int width, int height, CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood, int[] stayAlives , int[] becomeLives) {//f.e. stayAlives = 23 , becomeLive = 3	
		super(width, height, cellStateFactory, cellNeighborhood);
		
		stayAlive = new int[stayAlives.length];
		for(int i=0;i<stayAlives.length;i++)
			stayAlive[i]=stayAlives[i];
		becomeLive=new int[becomeLives.length];
		for(int i =0 ; i <becomeLive.length;i++)
			becomeLive[i]=becomeLives[i];

	}

	@Override
	protected Automaton newInstance(CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		return new GameOfLife(getWidth(),getHeight(),cellStateFactory,cellNeighborhood,stayAlive,becomeLive);
	}

	@Override
	protected CellState nextCellState(Cell currentCell, Set<Model.Cell> neighborsStates) {//rules
		int counter=0;
		CellState currentState=currentCell.state;
		
		
		Integer[] amountOfStates = {0,0,0,0}; //red,yellow,green,blue
		for(Model.Cell el : neighborsStates){
			if(el.state instanceof BinaryState){
				if(el.state.equals(BinaryState.ALIVE))
					counter++;
			}
			else{
				
				if(el.state.equals(QuadState.RED)){
					amountOfStates[0]++;
					counter++;
					
				}
				else if(el.state.equals(QuadState.YELLOW)){
					amountOfStates[1]++;
					counter++;
				}
				else if(el.state.equals(QuadState.GREEN)){
					amountOfStates[2]++;
					counter++;
				}
				else if(el.state.equals(QuadState.BLUE)){
					amountOfStates[3]++;
					counter++;
				}
			}
		}
		//JOptionPane.showMessageDialog(null, "RYGB" + amountOfStates[0]+" "+ amountOfStates[1]+" " + amountOfStates[2]+" " + amountOfStates[3]);
		if(currentState == BinaryState.DEAD || currentState==QuadState.DEAD){//red,yellow,green,blue
			for(int el : becomeLive){
				if(counter == el){
					if(StartWindow.startData.getVariant().equals("quad life")){
						int idx=0;
						for(int i =0 ; i<amountOfStates.length;i++){//jezeli jeden z 3 sasiadow jest wiecej to ten
							if(amountOfStates[i]>1){
								idx=i;
								break;
							}
							if(amountOfStates[i].equals(0)){//przy 3 sasiadach sie zgadza , 3 roznych sasiadow , to ten ktorego nie ma
								idx=i;
							}
						}
						
						if(idx==0)
							return QuadState.RED;
						else if(idx==1)
							return QuadState.YELLOW;
						else if(idx==2)
							return QuadState.GREEN;
						else 
							return QuadState.BLUE;
					}
					else
						return BinaryState.ALIVE;
				}
			}
			if(StartWindow.startData.getVariant().equals("quad life"))
				return QuadState.DEAD;
			else
				return BinaryState.DEAD;
		}
		else{
			boolean found = false;
			for(int el : stayAlive){
				if(counter == el){
					found=true;
					break;
				}				
			}
			if(!found){
				if(StartWindow.startData.getVariant().equals("quad life"))
					return QuadState.DEAD;
				else
					return BinaryState.DEAD;
			}
			else
				return currentState;
		}

	}

}
