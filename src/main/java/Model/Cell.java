package Model;

//final class can't be extended
/**
 * 
 * Klasa przechowujaca koordynat i stan danej komorki gry
 * @author Dominik Katszer
 *
 */
public class Cell implements Comparable<Cell>{ 
	public CellState state;
	public CellCoordinates coords;
	/**
	 * Konstruktor
	 * @param koordynat komorki
	 * @param stan komorki
	 */
	public Cell(CellCoordinates cc , CellState cs){
		coords=cc;
		state=cs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coords == null) ? 0 : coords.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cell other = (Cell) obj;
		if (coords == null) {
			if (other.coords != null)
				return false;
		} else if (!coords.equals(other.coords))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		return true;
	}

	@Override
	public int compareTo(Cell arg0) {
		if(coords instanceof Coords2D){
			Coords2D first = (Coords2D)coords;
			Coords2D second = (Coords2D)arg0.coords;
			
			if(first.y > second.y)
				return 1;
			else if(first.y < second.y)
				return -1;
			else{
				if(first.x>second.x)
					return 1;
				else if(first.x<second.x)
					return -1;
				else
					return 0;
			}
		}
		else{
			Coords1D first = (Coords1D)coords;
			Coords1D second = (Coords1D)arg0.coords;
			if(first.x>second.x)
				return 1;
			else if(first.x<second.x)
				return -1;
			else
				return 0;
		}
	}

	@Override
	public String toString() {
		return "Cell [state=" + state + ", coords=" + coords + "]";
	}
	
	
	
	
}
