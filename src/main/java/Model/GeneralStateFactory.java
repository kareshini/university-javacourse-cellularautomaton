package Model;

import java.util.HashMap;
import java.util.Map;
/**
 * Klasa inicjalizujaca na podstawie przekazanej mapy 
 * @author Dominik Katszer
 *
 */
public class GeneralStateFactory implements CellStateFactory {

	private Map<CellCoordinates,CellState> states;
	private Map<CellCoordinates,CellState> cells = new HashMap<CellCoordinates,CellState>();
	private CellState other;
	@Override
	public void initialState(CellCoordinates cellCoords) {
		if(cells.containsKey(cellCoords)){
			cells.put(cellCoords, states.get(cellCoords));
		}
		else
			cells.put(cellCoords, other);
	}
	/**
	 * konstrukotr
	 * @param initMapStructure mapa inicjalizujaca
	 * @param other stan okreslajacy co ma zrobic w przypadku gdy nie natrafi na komroke ktora nie nalezy do mapy inicjalizujacej
	 */
	public GeneralStateFactory(Map<CellCoordinates,CellState> initMapStructure , CellState other){
		states = initMapStructure;
		this.other = other;
	}
	@Override
	public Map<CellCoordinates,CellState> getMap(){
		return cells;
	}

}
