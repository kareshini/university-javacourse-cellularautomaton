package Model;
/**
 * Enum reprezentujaca czy komorka jest zywa czy martwa
 * @author Dominik Katszer
 *
 */
public enum BinaryState implements CellState {
	DEAD , ALIVE ;
}
