package Model;
/**
 * Interface reprezentujacy koordynat gry
 * @author Dominik Katszer
 *
 */
public interface CellCoordinates {
	@Override
	public boolean equals(Object other);
}
