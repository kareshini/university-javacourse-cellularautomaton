package Model;

import java.util.Map;
/**
 * Interface obiektu inicjalizujacego stan gry
 * @author Dominik Katszer
 *
 */
public interface CellStateFactory {
	/**
	 * inicjlizuje stan komorki
	 * @param cellCoords koordynat komorki
	 */
	public void initialState(CellCoordinates cellCoords);
	/**
	 * getter
	 * @return zainicjalizowana mapa gry
	 */
	public Map<CellCoordinates,CellState> getMap();
}
