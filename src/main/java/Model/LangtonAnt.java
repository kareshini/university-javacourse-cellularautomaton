package Model;

import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;
/**
 * Gra dwuwymiarowa, mrowka langtona . Reprezentuje pojedynczy stan tej gry
 * @author Dominik Katszer
 *
 */
public class LangtonAnt extends Automaton2Dim {
	private int w;
	private int h;
	private Set<CellCoordinates> setAntChanged = new HashSet<CellCoordinates>();
	//private boolean mapIsCreated=false
	/**
	 * Konstrktor
	 * @param width szerokosc map gry
	 * @param height wysokosc mapy gry
	 * @param cellStateFactory obiekt inicjalizujacy
	 * @param cellNeighborhood rodzaj sasiedztwa
	 */
	public LangtonAnt(int width, int height, CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		super(width, height, cellStateFactory, cellNeighborhood);
		w=width;
		h=height;
	}

	@Override
	protected Automaton newInstance(CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		return new LangtonAnt(getWidth(),getHeight(),cellStateFactory,cellNeighborhood);
	}

	@Override
	protected CellState nextCellState(Cell currentCell, Set<Cell> neighborsStates) {

		LangtonCell currentState = (LangtonCell) currentCell.state;
		for(Cell el :neighborsStates){
		//	JOptionPane.showMessageDialog(null,el.coords); sasiedztwo jest ok
		//	if(((LangtonCell)el.state).antState==null)
		//		JOptionPane.showMessageDialog(null,"null antState w sasiedztwie");
			if(((LangtonCell)el.state).antState!=AntState.NONE){
				if(nextAntCoordsIsThisCell(currentCell,el)){//funkcja ta obraca mrowki i sprawdza przesuniecie
					return new LangtonCell(currentState.cellState, ((LangtonCell)el.state).antId,((LangtonCell)el.state).antState);//newAntState(((LangtonCell)el.state).antState, (currentState.cellState==BinaryState.ALIVE)?true:false)
				}
			}
		}
		if(currentState.antState==AntState.NONE){
			return currentState;		
		}
		else{
			return new LangtonCell((currentState.cellState==BinaryState.ALIVE)?BinaryState.DEAD:BinaryState.ALIVE, null, AntState.NONE);
		}
	
		
		/*	if(currentState.antState==AntState.NONE){
			for(LangtonCell el : ants.keySet()){
				if(ants.get(el).equals(currentCell.coords)){
					return new LangtonCell((BinaryState) currentCell.state, el.antId, el.antState);
				}
			}
			return currentCell.state;
		}
		else{
			if(currentState.cellState==BinaryState.ALIVE){//black , 90 degrees right and move
				
				ants.put(new LangtonCell(null, currentState.antId, newAntState(currentState.antState,true)), nextAntCoords(neighborsStates,currentState.antState,(Coords2D)currentCell.coords));//replace
				return new LangtonCell(BinaryState.DEAD, null, null);
			}
			else{
				ants.put(new LangtonCell(null, currentState.antId, newAntState(currentState.antState,false)), nextAntCoords(neighborsStates,currentState.antState,(Coords2D)currentCell.coords));//replace
				return new LangtonCell(BinaryState.ALIVE, null, null);
			}
		}*/
	}
	private AntState newAntState(AntState currentState,boolean turnRight){
		int tmp=currentState.direct;
		if(turnRight){
			tmp++;
			if(tmp>4)
				tmp=1;
		}
		else{
			tmp--;
			if(tmp<1)
				tmp=4;
		}
		
		if(tmp==1)
			return AntState.NORTH;
		else if(tmp==2)
			return AntState.EAST;
		else if(tmp==3)
			return AntState.SOUTH;
		else 
			return AntState.WEST;
	}
	/*private CellCoordinates nextAntCoords(Set<Cell> neighborsStates,AntState antState,Coords2D currentCoords){
		Set<Coords2D> neighborsCoords = new HashSet<Coords2D>();
		for(Cell el:neighborsStates){
			neighborsCoords.add((Coords2D) el.coords);
		}
		
		
		
		if(antState == AntState.NORTH){
			for(Coords2D el:neighborsCoords){
				if(el.x==currentCoords.x){
					if(el.y==currentCoords.y-1 || el.y==currentCoords.y+h-1)
						return el;
				}
			}
		}
		else if(antState == AntState.EAST){
			for(Coords2D el:neighborsCoords){
				if(el.y==currentCoords.y){
					if(el.x==currentCoords.x+1 || el.x==currentCoords.x-w+1)
						return el;
				}
			}
		}
		else if(antState == AntState.SOUTH){
			for(Coords2D el:neighborsCoords){
				if(el.x==currentCoords.x){
					if(el.y==currentCoords.y+1 || el.y==currentCoords.y-h+1)
						return el;
				}
			}
		}
		else if(antState == AntState.WEST){
			for(Coords2D el:neighborsCoords){
				if(el.y==currentCoords.y){
					if(el.x==currentCoords.x-1 || el.x==currentCoords.x+w-1)
						return el;
				}
			}
		}

		return null;
	}
	*/
	private boolean nextAntCoordsIsThisCell(Cell current,Cell next){
		LangtonCell state = (LangtonCell)next.state;
		
		if(!setAntChanged.contains(next.coords)){//jezeli mrowka nie byla obracana to nie ma jej w secie 
			setAntChanged.add(next.coords); //bycie w liscie oznacza ze dana mrowka zostala obrocona, przy kolejnym sprawdzaniu sasiedztwa nie zostanie ona obrocona znowu
			state.antState=newAntState(state.antState,(state.cellState==BinaryState.DEAD)?true:false);
		}
		
		Coords2D  nextCoords = (Coords2D)next.coords;
		Coords2D  currentCoords = (Coords2D)current.coords;
		
//		JOptionPane.showMessageDialog(null, "current : " + currentCoords + " | sasiad :" + nextCoords);
		if(state.antState == AntState.NORTH){
			if(currentCoords.x==nextCoords.x){
				if(nextCoords.y==currentCoords.y+1 || nextCoords.y==currentCoords.y-h+1)
						return true;
			}
		}
		else if(state.antState == AntState.EAST){
			if(currentCoords.y==nextCoords.y){
				if(nextCoords.x==currentCoords.x-1 || nextCoords.x==currentCoords.x+w-1)
						return true;
			}
		}
		else if(state.antState == AntState.SOUTH){
			if(currentCoords.x==nextCoords.x){
				if(nextCoords.y==currentCoords.y-1 || nextCoords.y==currentCoords.y+h-1)
						return true;
			}
		}
		else if(state.antState == AntState.WEST){
			if(currentCoords.y==nextCoords.y){
				if(nextCoords.x==currentCoords.x+1 || nextCoords.x==currentCoords.x-w+1)
						return true;
			}
		}
		return false;
	}

}
