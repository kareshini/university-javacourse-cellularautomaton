package Model;
/**
 * Klasa reprezentujaca pojedyncza komrke gry mrowki langtona
 * @author Dominik Katszer
 *
 */
public class LangtonCell implements CellState {
	public BinaryState cellState;
	public Integer antId;
	public AntState antState;

	/**
	 * konstruktor
	 * @param _cellState stan komorki dead or alive
	 * @param _antId id mrowki
	 * @param _antState stan mrowki , albo brak brak mrowki
	 */
	public LangtonCell(BinaryState _cellState,Integer _antId ,AntState _antState){
		cellState=_cellState;
		antId=_antId;
		antState=_antState;
	}
	
	@Override
	public String toString(){
		return cellState+" : Ant ("+antState+")";	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((antState == null) ? 0 : antState.hashCode());
		result = prime * result + ((cellState == null) ? 0 : cellState.hashCode());
		return result;
	}

	@Override//bez id z powodu ze w games states factory ustalam ze id jest nie wazne do kolorowania
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LangtonCell other = (LangtonCell) obj;
		if (antState != other.antState)
			return false;
		if (cellState != other.cellState)
			return false;
		return true;
	}

	
}
