package Model;

import java.util.Set;

import javax.swing.JOptionPane;
/**
 * Gra wireworl dwuwymiarowa. Opisuje pojedynczy stan tej gry.
 * @author Dominik Katszer
 *
 */
public class WireWorld extends Automaton2Dim {
	/**
	 * konstruktor
	 * @param width szerokosc mapy gry
	 * @param height wysokosc mapy gry
	 * @param cellStateFactory obiekt inicjalizujacy
	 * @param cellNeighborhood sasiedztwo
	 */
	public WireWorld(int width, int height, CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		super(width, height, cellStateFactory, cellNeighborhood);
	}

	@Override
	protected Automaton newInstance(CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		return new WireWorld(getWidth(),getHeight(),cellStateFactory,cellNeighborhood);
	}

	@Override
	protected CellState nextCellState(Cell currentCell, Set<Cell> neighborsStates) {
		CellState currentState=currentCell.state;
		if(currentState.equals(WireElectronState.ELECTRON_HEAD))
			return WireElectronState.ELECTRON_TAIL;
		else if(currentState.equals(WireElectronState.ELECTRON_TAIL))
			return WireElectronState.WIRE;
		else if(currentState.equals(WireElectronState.VOID))
			return WireElectronState.VOID;
		else if(currentState.equals(WireElectronState.WIRE)){
			int count=0;
			for(Cell el :neighborsStates){
			//	JOptionPane.showMessageDialog(null,"neighbors of " + currentCell + " : " +el);
				if(el.state.equals(WireElectronState.ELECTRON_HEAD))
					count++;
			}
			if(count==1 || count==2){
			//	JOptionPane.showMessageDialog(null,currentCell + "change to ELECTRON_HEAD");
				return WireElectronState.ELECTRON_HEAD;
			}
			else{
			//	JOptionPane.showMessageDialog(null,currentCell + "change to WIRE");
				return WireElectronState.WIRE;
			}
		}
		return null;
	}

}
