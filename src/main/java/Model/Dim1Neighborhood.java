package Model;

import java.util.HashSet;
import java.util.Set;
/**
 * Sasiedztwo jednowymiarowe
 * @author Dominik Katszer
 *
 */
public class Dim1Neighborhood implements CellNeighborhood {

	private int radius;
	private boolean wrapping;
	private int size;
	
	@Override
	public Set<CellCoordinates> cellNeighbors(CellCoordinates cell) {
		Set<Coords1D> tmp = new HashSet<Coords1D>();
		Set<CellCoordinates> returned = new HashSet<CellCoordinates>();
		
		Coords1D coord = (Coords1D) cell;
		for(int i=-radius;i<=radius;i++){
			if(i==0)
				continue;
			tmp.add(new Coords1D(coord.x+i));
		}
		if(!wrapping){
			for(Coords1D el : tmp){
				if(el.x<0 || el.x>=size)
					continue;
				returned.add(el);
			}
		}
		else{
			for(Coords1D el : tmp){
				if(el.x<0 )
					returned.add(new Coords1D(el.x+size));
				else if(el.x>=size)
					returned.add(new Coords1D(el.x-size));
				else
					returned.add(el);
			}
		}
		
		return returned;
	}
	/**
	 * konstruktor
	 * @param rad szerokosc sasiedztwa
	 * @param wrap czy ma byc opcja zawijania
	 * @param size rozmiar gry
	 */
	public Dim1Neighborhood(int rad,boolean wrap , int size){
		radius=rad;
		wrapping=wrap;
		this.size=size;
	}

}
