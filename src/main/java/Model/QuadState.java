package Model;
/**
 * Enum reprezentujacy Stany Gry w zycie wersji QuadState
 * @author Dominik Katszer
 *
 */
public enum QuadState implements CellState {
	DEAD , RED , YELLOW , BLUE, GREEN ;
}
