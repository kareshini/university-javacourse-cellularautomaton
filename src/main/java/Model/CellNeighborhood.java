package Model;

import java.util.Set;
/**
 * Interfejs sasiedztw
 * @author Dominik Katszer
 *
 */
public interface CellNeighborhood {
	public Set<CellCoordinates> cellNeighbors(CellCoordinates cell);
}
