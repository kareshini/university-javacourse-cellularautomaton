package Model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * Gra jednowymiarowa Rule. Obrazuje jedynie stan pojedynczego stanu gry
 * @author Dominik Katszer
 *
 */
public class Rule extends Automaton1Dim {
	Map<Integer,CellState> isLive = new HashMap<Integer,CellState>();
	Integer[] tabNeighborsValue = {7,3,5,1,6,2,4,0};
	Integer variant;
	String bajt;
	/**
	 * konstrukotr
	 * @param size rozmiar gry
	 * @param cellStateFactory obiekt inicjalizujacy
	 * @param cellNeighborhood sasiedztwo
	 * @param variant wariant gry
	 */
	public Rule(int size, CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood,Integer variant) {
		super(size, cellStateFactory, cellNeighborhood);
		
		this.variant=variant;
		String tmp=Integer.toBinaryString(variant);
		StringBuilder binary = new StringBuilder();
		for(int i=0;i<8-tmp.length();i++){
			binary.append('0');
		}
		binary.append(tmp);
		bajt=binary.toString();
		for(int i=0; i<bajt.length();i++){
			if(bajt.charAt(i)=='0')
				isLive.put(tabNeighborsValue[i], BinaryState.DEAD);
			else
				isLive.put(tabNeighborsValue[i], BinaryState.ALIVE);
		}
	}

	@Override
	protected Automaton newInstance(CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		return new Rule(getSize(), cellStateFactory, cellNeighborhood, this.variant);
	}

	@Override
	protected CellState nextCellState(Cell currentState, Set<Cell> neighborsStates) {
		//if cell = Live then Left = 1 , Current = 2 , Right = 4
		Integer result=0;

		for(Cell el:neighborsStates){
			if(((Coords1D)el.coords).x==((Coords1D)currentState.coords).x-1 || ((Coords1D)el.coords).x==((Coords1D)currentState.coords).x+getSize()-1){//Left
				if(el.state==BinaryState.ALIVE)
					result+=1;
			}
			else{//Right , in neighborsStates isn't currentState
				if(el.state==BinaryState.ALIVE)
					result+=4;
			}
		}
		if(currentState.state==BinaryState.ALIVE)
			result+=2;
	//	System.out.println(result + " + " +currentState.coords);
		return isLive.get(result);
	}

}
