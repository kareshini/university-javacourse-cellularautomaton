package Model;
/**
 * Klasa reprezentujaca wszystkie gry jednowymiarowe
 * @author Dominik Katszer
 *
 */
public abstract class Automaton1Dim extends Automaton {

	private int size;
	/**
	 * Konstruktor gier jednywymiarowych
	 * @param size rozmiar gry
	 * @param cellStateFactory Obiekt inicjalizujacy 
	 * @param cellNeighborhood Rodzaj sasiedztwa
	 */
	public Automaton1Dim(int size, CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		super(cellStateFactory, cellNeighborhood);
		
		this.size=size;
		for(int i = 0 ; i<size; i++)
			cellStateFactory.initialState(new Coords1D(i));
		this.InitCells(cellStateFactory.getMap());
	}

	@Override
	protected boolean hasNextCoordinates(CellCoordinates cellCoordiantes) {
		Coords1D tmp = (Coords1D) cellCoordiantes;
		
		if(tmp.x>=size)
			return false;
		if(tmp.x<0)
			return false;
		return true;
	}

	@Override
	protected CellCoordinates initialCoordinates() {
		return new Coords1D(-1);
	}

	@Override
	protected CellCoordinates nextCoordinates(CellCoordinates cellCoordiantes) {
		return new Coords1D(((Coords1D)cellCoordiantes).x + 1);
	}
	
	/**
	 * Getter do rozmiaru
	 * @return rozmiar automatona
	 */
	public int getSize(){
		return size;
	}
}
