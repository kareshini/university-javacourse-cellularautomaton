package Model;

import java.util.HashSet;
import java.util.Set;
/**
 * Sasiedztwo von Neuman
 * @author Dominik Katszer
 *
 */
public class VonNeumanNeighborhood implements CellNeighborhood {
	private int rad;
	private boolean wrap;
	private int width;
	private int height;
	
	@Override
	public Set<CellCoordinates> cellNeighbors(CellCoordinates cell) {
		Set<Coords2D> tmp = new HashSet<Coords2D>();
		Set<CellCoordinates> returned = new HashSet<CellCoordinates>();
	
		Coords2D coord = (Coords2D) cell;

		for(int i=1 ; i<=rad ; i++){
			for(int j=1;j<i;j++){//krawedzie bez naroznikow
				tmp.add(new Coords2D(coord.x - i + j , coord.y + j ));
				tmp.add(new Coords2D(coord.x - i + j , coord.y - j ));
				tmp.add(new Coords2D(coord.x + i - j , coord.y + j ));
				tmp.add(new Coords2D(coord.x + i - j , coord.y - j ));
			}
			//narozniki
			tmp.add(new Coords2D(coord.x - i , coord.y ));
			tmp.add(new Coords2D(coord.x + i , coord.y ));
			tmp.add(new Coords2D(coord.x , coord.y - i ));
			tmp.add(new Coords2D(coord.x , coord.y + i ));
		}
		
		if(!wrap){
			for(Coords2D c : tmp){
				if(c.x<0 || c.x>=width || c.y<0 || c.y>=height)
					continue;		
				returned.add(c);
			}
			
		}
		else{
			for(Coords2D c : tmp){
				int xi=0,yi=0;
				if(c.x<0){
					xi=+1;
				}
				else if(c.x>=width){
					xi=-1;
				}
				if(c.y<0)
					yi=+1;
				else if(c.y>=height)
					yi=-1;
				
				returned.add(new Coords2D(c.x +(xi * width),c.y + (yi * height)));
			}
		}
		
	
		return returned;
	}
	/**
	 * konstruktor
	 * @param radius promien sasiedztwa
	 * @param wrapping czy z zawijaniem
	 * @param widthBoard szerokosc mapy gry
	 * @param heightBoard wysokosc mapy gry
	 */
	public VonNeumanNeighborhood(int radius ,boolean wrapping,int widthBoard , int heightBoard){
		rad = radius;
		wrap = wrapping ;
		width = widthBoard;
		height = heightBoard;
	}

}
