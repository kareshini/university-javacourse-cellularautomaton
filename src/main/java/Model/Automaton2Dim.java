package Model;
/**
 * Automaton reprezentujacy wszystkie gry dwuwymiarowe
 * @author Dominik Katszer
 *
 */
public abstract class Automaton2Dim extends Automaton {
	private int width;
	private int height;
		/**
		 * Konstruktor
		 * @param width szerokosc gry
		 * @param height wysokosc gry
		 * @param cellStateFactory Obiekt inicjalizujacy
		 * @param cellNeighborhood Sasiedztwo
		 */
	public Automaton2Dim(int width, int height, CellStateFactory cellStateFactory, CellNeighborhood cellNeighborhood) {
		super(cellStateFactory, cellNeighborhood);
		this.width=width;
		this.height=height;
		
		for(int i=0 ; i<width ; i++){
			for(int j = 0 ; j<height ; j++){
				cellStateFactory.initialState(new Coords2D(i,j));
			}
		}
		this.InitCells(cellStateFactory.getMap());
	}

	@Override
	protected boolean hasNextCoordinates(CellCoordinates cellCoordinates) {
		Coords2D tmp = (Coords2D) cellCoordinates;
		
		if(tmp.x>=width)
			return false;
		else if(tmp.x + 1 >=width){
			if(tmp.y + 1 <height )
				return true;
			else
				return false;
		}
		else if(tmp.y>=height)
			return false;
		else
			return true;
	}

	@Override
	protected CellCoordinates initialCoordinates() {
		return new Coords2D(-1,0);
	}

	@Override
	protected CellCoordinates nextCoordinates(CellCoordinates cellCoordiantes) {
		Coords2D tmp = (Coords2D) cellCoordiantes;
		int nx=tmp.x, ny=tmp.y;
		if(tmp.x==width-1){
			nx=0;
			ny++;
		}
		else{
			nx++;
		}
		return new Coords2D(nx,ny);
	}
	
	//auxiliry
	/**
	 * Getter
	 * @return szerokosc gry
	 */
	public int getWidth(){
		return width;
	}
	/**
	 * Getter
	 * @return wysokosc gry
	 */
	public int getHeight(){
		return height;
	}
}
