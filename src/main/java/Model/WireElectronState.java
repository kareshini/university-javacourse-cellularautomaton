package Model;
/**
 * Enum reprezentujacy stany komorek w grze WireWorld
 * @author Dominik Katszer
 *
 */
public enum WireElectronState implements CellState {
	VOID , WIRE , ELECTRON_HEAD , ELECTRON_TAIL ;
}
