package Model;

import java.util.HashSet;
import java.util.Set;
/**
 * Sasiedztwo moora
 * @author Dominik Katszer
 *
 */
public class MoorNeighborhood implements CellNeighborhood {
	private int rad;
	private boolean wrap;
	private int width;
	private int height;
	
	@Override
	public Set<CellCoordinates> cellNeighbors(CellCoordinates cell) {
		Set<CellCoordinates> returned = new HashSet<CellCoordinates>();
		
		if(cell instanceof Coords2D){
			Set<Coords2D> tmp = new HashSet<Coords2D>();
			
			
			Coords2D coord = (Coords2D) cell;
			for(int i = - rad ; i<=rad ; i++){
				for(int j = - rad ; j<=rad ; j++){
					if(i==0 && j ==0)
						continue;
					tmp.add(new Coords2D(coord.x + i, coord.y + j));
				}
			}		
			
			if(!wrap){
				for(Coords2D c : tmp){
					if(c.x<0 || c.x>=width || c.y<0 || c.y>=height)
						continue;		
					returned.add(c);
				}
			}
			else{
				for(Coords2D c : tmp){
					int xi=0,yi=0;
					if(c.x<0){
						xi=+1;
					}
					else if(c.x>=width){
						xi=-1;
					}
					if(c.y<0)
						yi=+1;
					else if(c.y>=height)
						yi=-1;
					
					returned.add(new Coords2D(c.x +(xi * width),c.y + (yi * height)));
				}
			}
		}
		else{
			Set<Coords1D> tmp = new HashSet<Coords1D>();
			Coords1D coord = (Coords1D) cell;
			tmp.add(new Coords1D(coord.x-1));
			tmp.add(new Coords1D(coord.x+1));
			
			if(!wrap){
				for(Coords1D c:tmp){
					if(c.x<0 || c.x>width)
						continue;
					returned.add(c);
				}
			}
			else{
				for(Coords1D c:tmp){
					if(c.x<0)
						returned.add(new Coords1D(c.x+width-1));
					else if(c.x>width)
						returned.add(new Coords1D(c.x-width+1));
					else
						returned.add(c);
				}
			}
		}
		
		return returned;
	}
	/**
	 * konstruktor
	 * @param radius promien 
	 * @param wrapping czy jest zawijanie w przyp[adku przekroczenia mapy
	 * @param widthBoard szerokosc planszy
	 * @param heightBoard wysokoscs planszy
	 */
	public MoorNeighborhood(int radius ,boolean wrapping,int widthBoard , int heightBoard){
		rad = radius;
		wrap = wrapping ;
		width = widthBoard;
		height = heightBoard;
		
	}

}
