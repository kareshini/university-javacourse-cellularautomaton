package Model;

/**
 * Enum okreslajacy ustawienie mrowki wzgledem stron swiata.
 * @author Dominik Katszer
 *
 */
public enum AntState {
	NONE(0) , NORTH(1) , SOUTH(3) , EAST(2) , WEST(4) ;
	
	public final int direct;
	AntState(int number){
		direct=number;
	}
}
