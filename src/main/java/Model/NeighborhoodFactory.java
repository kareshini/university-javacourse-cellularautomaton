package Model;

import View.StartWindow;
/**
 * Fabryka sasiedztw
 * @author Dominik Katszer
 *
 */
public class NeighborhoodFactory {
	/**
	 * Getter Fabryki
	 * @param czy zwrocone sasiedztwo ma byc sasiedztwem moora
	 * @return obiekt reprezentujacy
	 */
	public static CellNeighborhood getNeighborhood(boolean isMoore){
		if(isMoore)
			return new MoorNeighborhood(StartWindow.startData.getRadius(), StartWindow.startData.isWrapping(), StartWindow.startData.getWidth(), StartWindow.startData.getHeight());
		else
			return new VonNeumanNeighborhood(StartWindow.startData.getRadius(), StartWindow.startData.isWrapping(), StartWindow.startData.getWidth(), StartWindow.startData.getHeight());
	}
}
