package Model;

import org.junit.Assert;
import org.junit.Test;

public class Iterator1DimTest {
	private static Rule R;
	
	private void init(int size){
		R = new Rule(size, new UniformStateFactory(BinaryState.DEAD),new MoorNeighborhood(1, false, size, size),30);
	}
	
	@Test
	public void countOfIterates(){
		init(4);
		int counter = 0;
		for(Cell el : R){
			counter++;
			Assert.assertFalse(counter>4);
		}
	}
}
