package Model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class VonNeumanTest {
	private static int sizeOfBox;
	
	@BeforeClass
	public static void initSize(){
		sizeOfBox=10;
	}
	
	private void checkCrossingBoundary(Set<CellCoordinates> set , String error) {
		for(CellCoordinates el : set){
			Coords2D tmp = (Coords2D) el;
			Assert.assertTrue(error, tmp.x >=0 && tmp.x<sizeOfBox && tmp.y>=0 && tmp.y<sizeOfBox);
		}
	}
	private Set<CellCoordinates> Neighbors(int x, int y,int rad){
		VonNeumanNeighborhood neuman = new VonNeumanNeighborhood(rad, false, sizeOfBox, sizeOfBox);
		return neuman.cellNeighbors(new Coords2D(x,y));
	}
	
	@Test
	public void notOutOfBoundsRad1Test(){
		checkCrossingBoundary(Neighbors(2,2,1),"wrong notOutOfBounds");
	}

	
	
	@Test
	public void onlyLeftOutRad1Test(){
		checkCrossingBoundary(Neighbors(0,2,1),"wrong onlyLeft");
	}
	
	@Test
	public void onlyRightOutRad1Test(){
		checkCrossingBoundary(Neighbors(9,2,1),"wrong onlyRight");
	}
	
	@Test
	public void onlyTopOutRad1Test(){
		checkCrossingBoundary(Neighbors(2,0,1),"wrong onlyTop");
	}
	
	@Test
	public void onlyBottomOutRad1Test(){
		checkCrossingBoundary(Neighbors(2,9,1),"wrong onlyBottom");
	}
	
	@Test
	public void outOfBoardRad1Test(){
		checkCrossingBoundary(Neighbors(12,12,1),"wrong center");
	}
	
	@Test
	public void rightTopCornerRad1Test(){
		checkCrossingBoundary(Neighbors(9,0,1),"wrong RT-corner");
	}
	@Test
	public void rightBottomCornerRad1Test(){
		checkCrossingBoundary(Neighbors(9,9,1),"wrong RB-corner");
	}
	@Test
	public void LeftTopCornerRad1Test(){
		checkCrossingBoundary(Neighbors(0,0,1),"wrong LT-corner");
	}
	@Test
	public void LeftBottomCornerRad1Test(){
		checkCrossingBoundary(Neighbors(0,9,1),"wrong LB-corner");
	}
	
	@Test
	public void notOutOfBoundsRad2Test(){
		checkCrossingBoundary(Neighbors(2,2,2),"wrong notOutOfBounds");
	}

	
	
	@Test
	public void onlyLeftOutRad2Test(){
		checkCrossingBoundary(Neighbors(0,2,2),"wrong onlyLeft");
	}
	
	@Test
	public void onlyRightOutRad2Test(){
		checkCrossingBoundary(Neighbors(9,2,2),"wrong onlyRight");
	}
	
	@Test
	public void onlyTopOutRad2Test(){
		checkCrossingBoundary(Neighbors(2,0,2),"wrong onlyTop");
	}
	
	@Test
	public void onlyBottomOutRad2Test(){
		checkCrossingBoundary(Neighbors(2,9,2),"wrong onlyBottom");
	}
	
	@Test
	public void outOfBoardRad2Test(){
		checkCrossingBoundary(Neighbors(12,12,2),"wrong center");
	}
	
	@Test
	public void rightTopCornerRad2Test(){
		checkCrossingBoundary(Neighbors(9,0,2),"wrong RT-corner");
	}
	@Test
	public void rightBottomCornerRad2Test(){
		checkCrossingBoundary(Neighbors(9,9,2),"wrong RB-corner");
	}
	@Test
	public void LeftTopCornerRad2Test(){
		checkCrossingBoundary(Neighbors(0,0,2),"wrong LT-corner");
	}
	@Test
	public void LeftBottomCornerRad2Test(){
		checkCrossingBoundary(Neighbors(0,9,2),"wrong LB-corner");
	}
	//*******************************************************************************
	@Test
	public void ValuesNotCrossingRad1Test(){
		List<String> test = new LinkedList<String>();
		for(CellCoordinates el : Neighbors(4,4,2)){
			test.add(el.toString());
		}
		
		List<String> expected = Arrays.asList("2:4","3:3","3:4","3:5","4:2","4:3","4:5","4:6","5:3","5:4","5:5","6:4");
	
		for(String el : expected){
			Assert.assertTrue(test.contains(el));
		}
		Assert.assertTrue(test.size()==expected.size());
	}
	
	@Test
	public void ValuesNotWrappingLeftTopRad2Test(){
		List<String> test = new LinkedList<String>();
		for(CellCoordinates el : Neighbors(1,0,2)){
			test.add(el.toString());
		}
		List<String> expected = Arrays.asList("0:0","0:1","1:1","1:2","2:0","2:1","3:0");
	
		for(String el : expected){
			Assert.assertTrue("Not contain element : " +el ,test.contains(el));
		}
		Assert.assertTrue("Size is diferent",test.size()==expected.size());
	}
	
	@Test
	public void ValuesNotWrappingRightBottomRad2Test(){
		List<String> test = new LinkedList<String>();
		for(CellCoordinates el : Neighbors(9,9,2)){
			test.add(el.toString());
		}
		List<String> expected = Arrays.asList("7:9","8:9","8:8","9:8","9:7");
	
		for(String el : expected){
			Assert.assertTrue("Not contain element : " +el ,test.contains(el));
		}
		Assert.assertTrue("Size is diferent",test.size()==expected.size());
	}
	
	@Test
	public void ValuesWrappingLeftTopRad2Test(){
		List<String> test = new LinkedList<String>();
		VonNeumanNeighborhood von = new VonNeumanNeighborhood(2, true, sizeOfBox, sizeOfBox);
		
		for(CellCoordinates el : von.cellNeighbors(new Coords2D(1,0))){
			test.add(el.toString());
		}
		
		List<String> expected = Arrays.asList("0:0","0:1","1:1","1:2","2:0","2:1","3:0","9:0","0:9","1:9","1:8","2:9");

		for(String el : expected){
			Assert.assertTrue("Not contain element : " +el ,test.contains(el));
		}
		Assert.assertTrue("Size is diferent",test.size()==expected.size());
	}
	
	@Test
	public void ValuesWrappingRightBottomRad2Test(){
		List<String> test = new LinkedList<String>();
		
		VonNeumanNeighborhood von = new VonNeumanNeighborhood(2, true, sizeOfBox, sizeOfBox);
		
		for(CellCoordinates el : von.cellNeighbors(new Coords2D(9,9))){
			test.add(el.toString());
		}
		List<String> expected = Arrays.asList("7:9","8:9","8:8","9:8","9:7","0:9","0:8","1:9","8:0","9:0","9:1","0:0");
	
		for(String el : expected){
			Assert.assertTrue("Not contain element : " +el ,test.contains(el));
		}
		Assert.assertTrue("Size is diferent",test.size()==expected.size());
	}
}
