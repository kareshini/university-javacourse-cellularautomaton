package Model;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class CellStateFactoryInitialStateTest {
	@Test
	public void UniformTest(){
		UniformStateFactory cellStateFactory = new UniformStateFactory(BinaryState.DEAD);
		cellStateFactory.initialState(new Coords2D(5,5));
		cellStateFactory.initialState(new Coords2D(6,5));
		cellStateFactory.initialState(new Coords2D(7,5));
		cellStateFactory.initialState(new Coords2D(5,6));
		Assert.assertEquals("CellStateFactory size is wrong", cellStateFactory.getMap().size(),4);

		Set<CellCoordinates> keySet =  cellStateFactory.getMap().keySet();
		Set<Coords2D> setCoords2D = new HashSet<Coords2D>();
		for(CellCoordinates el:keySet){
			setCoords2D.add((Coords2D)el);
		}
		Assert.assertTrue("Doesn't contains 5,5 coords in Coords2DSet", setCoords2D.contains(new Coords2D(5,5)));
		Assert.assertTrue("Doesn't contains 6,5 coords in Coords2DSet", setCoords2D.contains(new Coords2D(6,5)));
		Assert.assertTrue("Doesn't contains 7,5 coords in Coords2DSet", setCoords2D.contains(new Coords2D(7,5)));
		Assert.assertTrue("Doesn't contains 5,6 coords in Coords2DSet", setCoords2D.contains(new Coords2D(5,6)));
		
		Assert.assertTrue("Doesn't contains 5,5 coords in cellStateFactory.getMap()", cellStateFactory.getMap().containsKey(new Coords2D(5,5)));
		Assert.assertTrue("Doesn't contains 6,5 coords in cellStateFactory.getMap()", cellStateFactory.getMap().containsKey(new Coords2D(6,5)));
		Assert.assertTrue("Doesn't contains 7,5 coords in cellStateFactory.getMap()", cellStateFactory.getMap().containsKey(new Coords2D(7,5)));
		Assert.assertTrue("Doesn't contains 5,6 coords in cellStateFactory.getMap()", cellStateFactory.getMap().containsKey(new Coords2D(5,6)));

		for(int i=0 ; i<10 ; i++){
			for(int j = 0 ; j<10 ; j++){
				cellStateFactory.initialState(new Coords2D(i,j));
			}
		}
		for(int i=0 ; i<10 ; i++){
			for(int j = 0 ; j<10 ; j++){
				Assert.assertTrue("initial in automaton2Dim is wrong", cellStateFactory.getMap().containsKey(new Coords2D(i,j)));
			}
		}
	}
	
	@Test
	public void GeneralTest(){
		
	}
}
