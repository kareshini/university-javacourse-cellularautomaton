package Model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AutomatonIteratorTest{
	private static GameOfLife GoL;
	private static CellStateFactory cellStateFactory ;
	private static int[] stayAlive;
	private static int[] becomeLive;

	@BeforeClass
	public static void init(){
		cellStateFactory = new UniformStateFactory(BinaryState.DEAD);
		String[] variant = "23/3".split("/");
		
		AutomatonIteratorTest.stayAlive = new int[variant[0].length()];
		AutomatonIteratorTest.becomeLive = new int[variant[1].length()];

		for(int i = 0 ; i <variant[0].length() ; i++){
			AutomatonIteratorTest.stayAlive[i]=Integer.parseInt(Character.toString(variant[0].charAt(i)));
		}
		for(int i = 0 ; i <variant[1].length() ; i++){
			AutomatonIteratorTest.becomeLive[i]=Integer.parseInt(Character.toString(variant[1].charAt(i)));
		}
	}
	private void init(int width,int height){
		GoL = new GameOfLife(width, height,   cellStateFactory ,new MoorNeighborhood(1, false, width, height),AutomatonIteratorTest.stayAlive ,AutomatonIteratorTest.becomeLive);
	}
	@Test
	public void initTest(){
		init(2,2);
		Assert.assertFalse("Init is wrong",GoL==null);
	}
	
	@Test
	public void CountOfIterations(){
		init(10,10);
		int count=0;
		for(Cell el:GoL){
			count++;
		//	System.out.println(el.coords); po kolei nam leci foreach
			Assert.assertFalse("Wrong number of iterations",count>100);
		}
	}
	@Test
	public void nextIteratorTest(){
		init(2,2);
		Iterator<Cell> it = GoL.iterator();
		Cell prev = it.next();
	
		Assert.assertTrue("Iterator aren't going forward", (((Coords2D)prev.coords).x+1)==(((Coords2D)it.next().coords).x));
		Assert.assertFalse("Iterator aren't going forward", prev.equals(it.next()));
	}
	@Test
	public void hasNextIteratorTest(){
		init(2,2);
		Iterator<Cell> it = GoL.iterator();
		int size = GoL.getMap().size();
		
		for(int i = 0 ; i<size ; i++){
			Assert.assertTrue("hasNextCoords wrong", it.hasNext());
			it.next();
		}
		
		//Assert.assertTrue("Wrong initial iterator", it.next().coords.equals(new Coords2D(0,0)));
	}
	@Test
	public void notNullIteratorTest(){
		init(2,2);
		Iterator<Cell> it = GoL.iterator();
		Assert.assertFalse("Iterator is null",it==null);
	}
	/*
	@Test
	public void setStateTest(){
		init(10,10);
		Map<CellCoordinates,CellState> init = new HashMap<CellCoordinates,CellState>();
		init.put(new Coords2D(0,0), BinaryState.ALIVE);
		init.put(new Coords2D(0,1), BinaryState.ALIVE);
		init.put(new Coords2D(1,0), BinaryState.ALIVE);
		init.put(new Coords2D(1,1), BinaryState.ALIVE);
		GoL.insertStructure(init);
		
	}
	*/
}
