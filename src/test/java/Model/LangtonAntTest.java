package Model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class LangtonAntTest {
	private LangtonAnt LA;
	private void init(int width,int height){
		
		LA = new LangtonAnt(width, height,   new UniformStateFactory(new LangtonCell(BinaryState.DEAD, null, AntState.NONE)) ,new VonNeumanNeighborhood(1, true, width, height));
	}
	
	@Test
	public void nextCellState1Test(){
		init(2,2);
		Set<Model.Cell> set = new HashSet<Model.Cell>();
		set.add(new Cell(new Coords2D(0,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)));
		set.add(new Cell(new Coords2D(1,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)));
		set.add(new Cell(new Coords2D(1,1),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)));
		//jest mrowka, zmien pole
		Assert.assertTrue("nextCellStateIsWrong",LA.nextCellState(new Cell(new Coords2D(0,1),new LangtonCell(BinaryState.DEAD, 0, AntState.NORTH)), set).equals(new LangtonCell(BinaryState.ALIVE, null, AntState.NONE)));
	}
	@Test
	public void nextCellState2Test(){
		init(2,2);
		Set<Model.Cell> set = new HashSet<Model.Cell>();
		set.add(new Cell(new Coords2D(0,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)));
		//set.add(new Cell(new Coords2D(1,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)));
		set.add(new Cell(new Coords2D(1,1),new LangtonCell(BinaryState.DEAD, 0, AntState.WEST)));
		set.add(new Cell(new Coords2D(0,1),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)));
		//wchodzi mrowka
		//System.out.println(LA.nextCellState(new Cell(new Coords2D(1,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)), set));
		//Assert.assertTrue("nextCellStateIsWrong",LA.nextCellState(new Cell(new Coords2D(1,1),new LangtonCell(BinaryState.DEAD, 0, AntState.WEST)), set).equals(new LangtonCell(BinaryState.ALIVE, null, AntState.NONE)));
		//System.out.println(((LangtonCell)LA.nextCellState(new Cell(new Coords2D(1,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)), set)).antState);
		Assert.assertTrue("nextCellStateAntStateIsWrong",((LangtonCell)LA.nextCellState(new Cell(new Coords2D(1,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE)), set)).equals(new LangtonCell(BinaryState.DEAD, null, AntState.NORTH)));
	}
	@Test
	public void nextState(){
		init(2,2);
		Map<Coords2D,LangtonCell> insertInit = new HashMap<Coords2D,LangtonCell>();
		insertInit.put(new Coords2D(0,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE));
		insertInit.put(new Coords2D(1,0),new LangtonCell(BinaryState.DEAD, null, AntState.NONE));
		insertInit.put(new Coords2D(0,1),new LangtonCell(BinaryState.DEAD, 0, AntState.WEST));
		insertInit.put(new Coords2D(1,1),new LangtonCell(BinaryState.DEAD, null, AntState.NONE));
		
		LA.insertStructure(insertInit);
		for(CellCoordinates el : LA.getMap().keySet()){
			System.out.println(el + " : " + insertInit.get(el));
		}
		
		System.out.println("***************");
		Automaton a =LA.nextState();
		for(CellCoordinates el : a.getMap().keySet()){
			System.out.println(el + " : " + a.getMap().get(el));
		}
		System.out.println("***************");
		a =a.nextState();
		for(CellCoordinates el : a.getMap().keySet()){
			System.out.println(el + " : " + a.getMap().get(el));
		}
		System.out.println("***************");
		a =a.nextState();
		for(CellCoordinates el : a.getMap().keySet()){
			System.out.println(el + " : " + a.getMap().get(el));
		}
	}
	
}
