package Model;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class RuleTest {
	private Rule R;
	private void init(int width,int height){
		
		R = new Rule(width, new UniformStateFactory(BinaryState.DEAD) ,new MoorNeighborhood(1, false, width, height),30);
	}

	@Test
	public void nextState(){
		init(7,1);
		Map<Coords1D,CellState> map = new HashMap<Coords1D,CellState>();
		map.put(new Coords1D(3),BinaryState.ALIVE);
	
		R.insertStructure(map);
		for(CellCoordinates el : R.getMap().keySet()){
			System.out.println(el + " : " + R.getMap().get(el));
		}
		
		System.out.println("***************");
		Automaton a =R.nextState();
		for(CellCoordinates el : a.getMap().keySet()){
			System.out.println(el + " : " + a.getMap().get(el));
		}
		System.out.println("***************");
		a =a.nextState();
		for(CellCoordinates el : a.getMap().keySet()){
			System.out.println(el + " : " + a.getMap().get(el));
		}
		System.out.println("***************");
		a =a.nextState();
		for(CellCoordinates el : a.getMap().keySet()){
			System.out.println(el + " : " + a.getMap().get(el));
		}
	}
}
