package Model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class GameOfLiveTest {
	private static GameOfLife GoL;
	private static CellStateFactory cellStateFactory ;
	private static int[] stayAlive;
	private static int[] becomeLive;

	@BeforeClass
	public static void init(){
		cellStateFactory = new UniformStateFactory(BinaryState.DEAD);
		String[] variant = "23/3".split("/");
		
		stayAlive = new int[variant[0].length()];
		becomeLive = new int[variant[1].length()];

		for(int i = 0 ; i <variant[0].length() ; i++){
			stayAlive[i]=Integer.parseInt(Character.toString(variant[0].charAt(i)));
		}
		for(int i = 0 ; i <variant[1].length() ; i++){
			becomeLive[i]=Integer.parseInt(Character.toString(variant[1].charAt(i)));
		}
	}
	private void init(int width,int height){
		GoL = new GameOfLife(width, height,   cellStateFactory ,new MoorNeighborhood(1, false, width, height),stayAlive ,becomeLive);
	}
	
	@Test
	public void createSize10x10Variant23x3Test(){
		init(10,10);
		
		Assert.assertTrue("new GoL is null", GoL!=null);
		Assert.assertEquals("Wrong size in 23x3 10x10", 10, GoL.getHeight());
		Assert.assertEquals("Wrong size in 23x3 10x10", 10, GoL.getWidth());
		System.out.println(GoL.getMap().size());
		Assert.assertEquals("Inner map is wrong (size)",GoL.getMap().size(),100);
		for(Cell el : GoL){
			Assert.assertTrue("State is null", el.state!=null);
		}
	}
	@Test
	public void insertStructureTest(){
		init(3,3);
		Map<CellCoordinates,CellState> init = new HashMap<CellCoordinates,CellState>();
		init.put(new Coords2D(0,0), BinaryState.ALIVE);
		init.put(new Coords2D(0,1), BinaryState.ALIVE);
		init.put(new Coords2D(1,0), BinaryState.ALIVE);
		init.put(new Coords2D(1,1), BinaryState.ALIVE);
		
		Map<CellCoordinates, CellState> prev = new HashMap<CellCoordinates,CellState>(GoL.getMap());
		for(Cell el: GoL){
			System.out.println(el);
		}
		GoL.insertStructure(init);
		System.out.println("****");
		for(Cell el: GoL){
			System.out.println(el);
		}
		Assert.assertFalse("Before and after insertStrucutres maps are equals",prev.equals(GoL.getMap()));
	}
	@Test
	public void nextStateBlockTest(){
		init(3,3);
		Map<CellCoordinates,CellState> init = new HashMap<CellCoordinates,CellState>();
		init.put(new Coords2D(0,0), BinaryState.ALIVE);
		init.put(new Coords2D(0,1), BinaryState.ALIVE);
		init.put(new Coords2D(1,0), BinaryState.ALIVE);
		init.put(new Coords2D(1,1), BinaryState.ALIVE);
	
		GoL.insertStructure(init);
		Map<CellCoordinates,CellState> formerState= GoL.getMap();
	/*	System.out.println("forEach GoL after insert");
		for(Cell el: GoL){
			System.out.println(el);
		}
		
		System.out.println("Former state");
		for(Cell el: GoL){
			System.out.println(el);
		}
		System.out.println("New state");
		for(Cell el: GoL.nextState()){
			System.out.println(el);
		}
	*/
		Assert.assertTrue("current map and next Map should be equals",formerState.equals(GoL.getMap()));
		
	}
	/*@Test
	public void nextStateBlinkerTest(){
		init(3,3);
		Map<CellCoordinates,CellState> init = new HashMap<CellCoordinates,CellState>();
		init.put(new Coords2D(2,1), BinaryState.ALIVE);
		init.put(new Coords2D(0,1), BinaryState.ALIVE);
		init.put(new Coords2D(1,1), BinaryState.ALIVE);
	
		GoL.insertStructure(init);
		Map<CellCoordinates,CellState> formerState= GoL.getMap();
		
		
		System.out.println("Former state");
		for(Cell el: GoL){
			System.out.println(el);
		}
		System.out.println("New state");
		for(Cell el: GoL.nextState()){
			System.out.println(el);
		}
	
		//Assert.assertFalse("current map and next Map shouldn't be equals",formerState.equals(GoL.getMap()));
		
	}*/
}
