package Model;

import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class Dim1NeighborhoodTest {
private static int sizeOfBox;
	
	@BeforeClass
	public static void initSize(){
		sizeOfBox=10;
	}
	
	private void checkCrossingBoundary(Set<CellCoordinates> set , String error) {
		for(CellCoordinates el : set){
			Coords1D tmp = (Coords1D) el;
			System.out.println(tmp.x);
			Assert.assertTrue(error, tmp.x >=0 && tmp.x<sizeOfBox);
		}
		System.out.println();
	}
	private Set<CellCoordinates> Neighbors(int x){
		Dim1Neighborhood dim = new Dim1Neighborhood(1, false, sizeOfBox);
		return dim.cellNeighbors(new Coords1D(x));
	}
	@Test
	public void notOutOfBoundsTest(){
		checkCrossingBoundary(Neighbors(2),"wrong notOutOfBounds");
	}
	@Test
	public void LeftOutTest(){
		checkCrossingBoundary(Neighbors(0),"wrong onlyLeft");
	}
	@Test
	public void RightOutTest(){
		checkCrossingBoundary(Neighbors(9),"wrong onlyLeft");
	}
}
