package Model;

import org.junit.Assert;
import org.junit.Test;

public class LangtonCellEqualsTest {

	@Test
	public void differetCellsTest(){
		LangtonCell a =new LangtonCell(BinaryState.DEAD, null, AntState.NONE);
		LangtonCell b =new LangtonCell(BinaryState.DEAD, 0, AntState.NONE);
		Assert.assertTrue(a.equals(b));
	}
}
