package Model;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class GameOfLifeNextStateTest {
	private static GameOfLife GoL;
	private static CellStateFactory cellStateFactory ;
	private static int[] stayAlive;
	private static int[] becomeLive;

	@BeforeClass
	public static void init(){
		cellStateFactory = new UniformStateFactory(BinaryState.DEAD);
		String[] variant = "23/3".split("/");
		stayAlive = new int[variant[0].length()];
		becomeLive = new int[variant[1].length()];

		for(int i = 0 ; i <variant[0].length() ; i++){
			stayAlive[i]=Integer.parseInt(Character.toString(variant[0].charAt(i)));
		}
		for(int i = 0 ; i <variant[1].length() ; i++){
			becomeLive[i]=Integer.parseInt(Character.toString(variant[1].charAt(i)));
		}
	}
	private void init(int width,int height){
		GoL = new GameOfLife(width, height,   cellStateFactory ,new MoorNeighborhood(1, false, width, height),stayAlive ,becomeLive);
	}
	@Test
	public void nextStateTest(){
		/*//po dodaniu quad life to nei dziala , bo operujemy na startGameDAta
		 ? 1
		 1 1
		 */
		/*
		init(2,2);
		Set<Model.Cell> set = new HashSet<Model.Cell>();
		set.add(new Cell(new Coords2D(0,1),BinaryState.ALIVE));
		set.add(new Cell(new Coords2D(1,0),BinaryState.ALIVE));
		set.add(new Cell(new Coords2D(1,1),BinaryState.ALIVE));
		*/
		
	//	Assert.assertTrue("stayAlive is wrong",GoL.nextCellState(new Cell(new Coords2D(0,0),BinaryState.ALIVE), set).equals(BinaryState.ALIVE));
	//	Assert.assertTrue("becomelive is wrong",GoL.nextCellState(new Cell(new Coords2D(0,0),BinaryState.DEAD), set).equals(BinaryState.ALIVE));

		/*
		 ? 0
		 1 0
		 */
		/*
		Set<Model.Cell> set2 = new HashSet<Model.Cell>();
		set2.add(new Cell(new Coords2D(0,1),BinaryState.DEAD));
		set2.add(new Cell(new Coords2D(1,0),BinaryState.ALIVE));
		set2.add(new Cell(new Coords2D(1,1),BinaryState.DEAD));
		
		Assert.assertTrue("stayAlive is wrong",GoL.nextCellState(new Cell(new Coords2D(0,0),BinaryState.ALIVE), set2).equals(BinaryState.DEAD));
		Assert.assertTrue("becomelive is wrong",GoL.nextCellState(new Cell(new Coords2D(0,0),BinaryState.DEAD), set2).equals(BinaryState.DEAD));
		*/
	}
}
