package Model;

import org.junit.Assert;
import org.junit.Test;

public class CellEqualsTest {
	@Test
	public void equalsTest(){
		Cell a = new Cell(new Coords2D(2,2),BinaryState.ALIVE);
		Cell b = new Cell(new Coords2D(2,2),BinaryState.ALIVE);
		Assert.assertTrue("Cell equals method doesn't work", a.equals(b));
	}
	
	@Test
	public void notEqualsCoordsTest(){
		Cell a = new Cell(new Coords2D(2,2),BinaryState.ALIVE);
		Cell b = new Cell(new Coords2D(3,2),BinaryState.ALIVE);
		Assert.assertTrue("Cell not equals method doesn't work", !a.equals(b));
	}
	@Test
	public void notEqualsStateTest(){
		Cell a = new Cell(new Coords2D(2,2),BinaryState.ALIVE);
		Cell b = new Cell(new Coords2D(2,2),BinaryState.DEAD);
		Assert.assertTrue("Cell not equals method doesn't work", !a.equals(b));
	}
	@Test
	public void notEqualsCoordsAndStateTest(){
		Cell a = new Cell(new Coords2D(1,2),BinaryState.ALIVE);
		Cell b = new Cell(new Coords2D(2,2),BinaryState.DEAD);
		Assert.assertTrue("Cell not equals method doesn't work", !a.equals(b));
	}
}

