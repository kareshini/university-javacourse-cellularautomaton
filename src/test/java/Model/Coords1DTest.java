package Model;

import org.junit.Assert;
import org.junit.Test;

public class Coords1DTest {
	@Test
	public void equalsTest(){
		Coords1D a=new Coords1D(2);
		Coords1D b = new Coords1D(2);
		Assert.assertTrue("method equals dosnt work", a.equals(b));
	}
	@Test
	public void notEqualsTest(){
		Coords1D a=new Coords1D(2);
		Coords1D b = new Coords1D(3);
		Assert.assertTrue("method not equals dosnt work", !a.equals(b));
	}
}
