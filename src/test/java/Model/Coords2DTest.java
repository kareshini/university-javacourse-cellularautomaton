package Model;

import org.junit.Assert;
import org.junit.Test;

public class Coords2DTest {
	@Test
	public void equalsTest(){
		Coords2D a=new Coords2D(2,2);
		Coords2D b = new Coords2D(2,2);
		Assert.assertTrue("method equals dosnt work", a.equals(b));
	}
	@Test
	public void notEqualsTest(){
		Coords2D a=new Coords2D(2,2);
		Coords2D b = new Coords2D(2,3);
		Assert.assertTrue("method not equals dosnt work", !a.equals(b));
	}
}
